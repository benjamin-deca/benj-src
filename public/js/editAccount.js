$(function () {
        $("#birthdate_form").datepicker($.datepicker.regional["fr"]);
		$("#since_asso_form").datepicker($.datepicker.regional["fr"]);
});

$(document).ready(function() {
	
	$("#submitCoord").on("click", function() {	
		var situation_maritale = $('#situation_maritale_form'),
			birthdate = $('#birthdate_form'),
			phone = $('#phone_form'),
			mobilephone = $('#mobilephone_form'),
			email = $('#email_form');
		var spinner = new Spinner(opts).spin(target);
			
		$.getJSON("modifyAccount",{ sit : situation_maritale.val(), bir : birthdate.val(), landlinephone : phone.val(), mobilephone : mobilephone.val(), email : email.val() })
		.success(function() {
			spinner.stop();
			$('#dialogForm').load('modifyCoord #dialogCoordOk', function(){
				$('#dialogForm').modal('show');
			});
		})
		.fail(function(jqxhr) {
			spinner.stop();
			var errors = jqxhr.responseJSON;
			$('#dialogForm').load('modifyCoord #dialogCoord', errors, function(){
				$('#dialogForm').modal('show');
				$.getScript('js/editAccount.js');
			});
		});
	});
	
	$("#submitAddress").on("click", function() {
		var address = $('#address_form'),
			zipcode = $('#zipcode_form'),
			city = $('#city_form'),
			country = $('#country_form');
		var spinner = new Spinner(opts).spin(target);
			
		$.getJSON("modifyAccount",{ address: address.val(), zipcode: zipcode.val(), city: city.val(), cou: country.val() })
		.success(function() {
			spinner.stop();
			$('#dialogForm').load('modifyAddress #dialogAddressOk', function(){
				$('#dialogForm').modal('show');
			});
		})
		.fail(function(jqxhr) {
			spinner.stop();
			var errors = jqxhr.responseJSON;
			$('#dialogForm').load('modifyAddress #dialogAddress', errors, function(){
				$('#dialogForm').modal('show');
				$.getScript('js/editAccount.js');
			});
		});
	});
	
	 $("#submitFoyer").on("click", function() {
		var civ = $('#civilite_form'),
			lastn = $('#lastname_form'),
			firstn = $('#firstname_form'),
			type_link = $('#type_link_form'),
			birthd = $('#birthdate_form'),
			birthp = $('#birthdatePlace_form');
		var spinner = new Spinner(opts).spin(target);
		
		$.getJSON("addLinkFoyer",{title : civ.val(), lastname : lastn.val(), firstname : firstn.val(), typelink : type_link.val(), birthdate : birthd.val(), birthplace : birthp.val()})
		.success(function() {
			spinner.stop();
			$('#dialogForm').load('addLinkFoy #dialogFoyerOk', function(){
				$('#dialogForm').modal('show');
			});
		})
		.fail(function(jqxhr) {
			spinner.stop();
			var errors = jqxhr.responseJSON;
			$('#dialogForm').load('addLinkFoy #dialogFoyer', errors, function(){
				$('#titleFoyer').empty();
				if(type_link.val() == 'Conjoint') {
					$('#titleFoyer').append('Ajouter un conjoint');
					$('#optConjoint').attr('selected', 'selected');
				} else if(type_link.val() == 'Enfant') {
					$('#titleFoyer').append('Ajouter un enfant');
					$('#optChild').attr('selected', 'selected');
				}
				$('#dialogForm').modal('show');
				$.getScript('js/editAccount.js');
			});
		});
	});
	
	$("#submitAsso").on("click", function() {
		var id_asso = $('#name_asso_form'),
			fonction = $('#fonction_asso_form'),
			since_asso = $('#since_asso_form');
		var spinner = new Spinner(opts).spin(target);
		
		$.getJSON("addLinkAssociation",{ id_asso: id_asso.val(), fonction: fonction.val(), since_asso: since_asso.val() })
		.success(function() {
			spinner.stop();
			$('#dialogForm').load('addLinkAsso #dialogAssoOk', function(){
				$('#dialogForm').modal('show');
			});
		})
		.fail(function(jqxhr) {
			spinner.stop();
			var errors = jqxhr.responseJSON;
			$.getJSON("getAssociation", function(data) {
				var $name_asso = $("#name_asso_form");
				$.each(data, function(index, value) {
					$name_asso.append('<option value="' + index +'">' + value + '</option>');
				});
			});
			$('#dialogForm').load('addLinkAsso #dialogAsso', errors, function(){
				$('#dialogForm').modal('show');
				$.getScript('js/editAccount.js');
			});
		});
		
	});
});