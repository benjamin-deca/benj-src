//	Main script of Decacity Portal

// Les options du Spinner
var opts = {
  lines: 13 // The number of lines to draw
, length: 28 // The length of each line
, width: 14 // The line thickness
, radius: 42 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
}

var target = document.getElementById('targetSpinner');

// Load the DataTable of account.blade.php's table
function LoadDatatableAccount() {
	
$('#tableFoyer').dataTable( {
    language: {
        processing:     "Traitement en cours...",
        search:         "",
        lengthMenu:     "Afficher _MENU_",
        info:           "Personnes _START_ &agrave; _END_ sur _TOTAL_",
        infoEmpty:      "",
        infoFiltered:   "(filtr&eacute; de _MAX_ personnes au total)",
        infoPostFix:    "",
        loadingRecords: "Chargement en cours...",
        zeroRecords:    "Aucune personne &agrave; afficher",
        emptyTable:     "Aucune personne &agrave; afficher",
        paginate: {
            first:      "Premier",
            previous:   "Pr&eacute;c&eacute;dent",
            next:       "Suivant",
            last:       "Dernier"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    },
	"aaSorting": [[ 1, "asc" ]],
	"sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
} );

$('#tableAsso').dataTable( {
    language: {
        processing:     "Traitement en cours...",
        search:         "",
        lengthMenu:     "Afficher _MENU_",
        info:           "Associations _START_ &agrave; _END_ sur _TOTAL_",
        infoEmpty:      "",
        infoFiltered:   "(filtr&eacute; de _MAX_ associations au total)",
        infoPostFix:    "",
        loadingRecords: "Chargement en cours...",
        zeroRecords:    "Aucune association &agrave; afficher",
        emptyTable:     "Aucune association &agrave; afficher",
        paginate: {
            first:      "Premier",
            previous:   "Pr&eacute;c&eacute;dent",
            next:       "Suivant",
            last:       "Dernier"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    },
	"aaSorting": [[ 0, "asc" ]],
	"sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
} );

$('#tableFoyer_filter input').attr("placeholder", "Rechercher");
$('#tableAsso_filter input').attr("placeholder", "Rechercher");
$('#tableFoyer_filter input').attr("class", "");
$('#tableAsso_filter input').attr("class", "");
	
};

// Dialog Box Account
function DialogAccount() {	
$("#modify-user").on("click", function() {
	$('#dialogForm').load('modifyCoord #dialogCoord', function(){
		$('#dialogForm').modal('show');
		$.getScript('js/datepicker-fr.js');
		$.getScript('js/editAccount.js');
	});
});
$("#modify-address").on("click", function() {
	$('#dialogForm').load('modifyAddress #dialogAddress', function(){
		$('#dialogForm').modal('show');
		$.getScript('js/editAccount.js');
	});
});

$("#add-conjoint").on("click", function() {
	$('#dialogForm').load('addLinkFoy #dialogFoyer', function(){
		$('#titleFoyer').empty();
		$('#titleFoyer').append('Ajouter un conjoint');
		$('#optConjoint').attr('selected', 'selected');
		$('#dialogForm').modal('show');
		$.getScript('js/datepicker-fr.js');
		$.getScript('js/editAccount.js');
	});
});
$("#add-child").on("click", function() {
	$('#dialogForm').load('addLinkFoy #dialogFoyer', function(){
		$('#titleFoyer').empty();
		$('#titleFoyer').append('Ajouter un enfant');
		$('#optChild').attr('selected', 'selected');
		$('#dialogForm').modal('show');
		$.getScript('js/datepicker-fr.js');
		$.getScript('js/editAccount.js');
	});
});
$("#add-asso").on("click", function() {
	$.getJSON("getAssociation", function(data) {
		var $name_asso = $("#name_asso_form");
		$.each(data, function(index, value) {
			$name_asso.append('<option value="' + index +'">' + value + '</option>');
		});
	});
	$('#dialogForm').load('addLinkAsso #dialogAsso', function(){
		$('#dialogForm').modal('show');
		$.getScript('js/datepicker-fr.js');
		$.getScript('js/editAccount.js');
	});
});
};

// DeleteAsso and DeleteFoyer functions
function DeleteAssoFoyer() {
$(".deleteLinkAssociation").on("click", function(){
	var r = confirm("Attention, vous allez supprimer un lien avec une association. \312tes-vous s\373r ?");
	if(r == true) {
		var spinner = new Spinner(opts).spin(target);
		var asso = $(".deleteLinkAssociation");
		$.getJSON("deleteLinkAssociation",{ idAsso : asso.val() })
		.success(function() {
			spinner.stop();
			window.location.reload();
		});
	}
});

$(".deleteLinkFoyer").on("click", function(){
	var r = confirm("Attention, vous allez supprimer un lien avec un membre de votre foyer. \312tes-vous s\373r ?");
	if(r == true) {
		var spinner = new Spinner(opts).spin(target);
		var contact = $(".deleteLinkFoyer");
		$.getJSON("deleteLinkFoyer",{ idContact : contact.val() })
		.success(function() {
			spinner.stop();
			window.location.reload();
		});
	}
});
};