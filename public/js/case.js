
$(document).ready(function() {
	
	$("#addCase").on("click", function() {	
		var situation_maritale = $('#situation_maritale_form'),
			birthdate = $('#birthdate_form'),
			phone = $('#phone_form'),
			mobilephone = $('#mobilephone_form'),
			email = $('#email_form');
		var spinner = new Spinner(opts).spin(target);
			
		$.getJSON("addCase",{ sit : situation_maritale.val(), bir : birthdate.val(), landlinephone : phone.val(), mobilephone : mobilephone.val(), email : email.val() })
		.success(function() {
			spinner.stop();
			$('#dialogForm').load('modifyCoord #dialogCoordOk', function(){
				$('#dialogForm').modal('show');
			});
		})
		.fail(function(jqxhr) {
			spinner.stop();
			var errors = jqxhr.responseJSON;
			$('#dialogForm').load('modifyCoord #dialogCoord', errors, function(){
				$('#dialogForm').modal('show');
				$.getScript('js/editAccount.js');
			});
		});
	});
});

function DialogCase() {
$("#add-case").on("click", function() {
	$('#dialogForm').load('submitCase #dialogCase', function(){
		$('#dialogForm').modal('show');
		$.getScript('js/datepicker-fr.js');
		$.getScript('js/case.js');
	});
});
}