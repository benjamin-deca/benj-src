<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});



/*
INSERT INTO `users` (`id`, `email`, `password`, `lastname`, `firstname`, `title`, `women_name`, `engaged_status`, `address`, `zipcode`, `city`, `country`, `landline_phone`, `mobile_phone`, `socialsecurity_id`, `birthdate`, `birthplace`, `externalcrm_id`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'testing@decacity.com', '$2y$10$X6dmav9YhiYMM3wi2tbqmuqqTXRIGFIclwJelIKXYnAM7kVbmTT8.', 'chaillet', 'benjamin', ' ', '', 'Célibataire', '6 Rue du Marais Sylvain', '41130', 'GIEVRES', 'FRA', '0101010200', '', '', '1987-09-26', '', '0032400000Bps8cAAB', '2015-09-14 17:06:55', '2015-09-20 13:02:11', '0ilZYzQ2dltbYSRXlzydX6Ytz0IMv5aHzcUg5IfNfjXLV9WTmQqCSGxUlQK0');

*/
$factory->defineAs(App\User::class, 'user_deca', function ($faker) {
    return [
        'lastname' => $faker->name,
        'firstname'=> $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10)
    ];
});