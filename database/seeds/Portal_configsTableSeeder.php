<?php
 
use Illuminate\Database\Seeder;
 
class Portal_configsTableSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('portal_configs')->delete();
 
        $portal_configs = array(
            ['id' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'MOD_BILL_ACTIVATED', 'config_value'=>'0', 'config_marker'=>'BILL', 'activated'=>1, 'council_activated'=>1, 'config_label'=>'Module Règlement', 'config_comments'=>'Activation du module règlement', 'council_enabled'=>1],
            ['id' => 2, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'MOD_EVENT_ACTIVATED', 'config_value'=>'0', 'config_marker'=>'EVENT', 'activated'=>1, 'council_activated'=>1, 'config_label'=>'Module Evenements', 'config_comments'=>'Activation du module Evènements', 'council_enabled'=>1],
            ['id' => 3, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'SFDC_USERNAME', 'config_value'=>'#SFDC_USERNAME#', 'config_marker'=>'SFDC', 'activated'=>1, 'council_activated'=>0, 'config_label'=>'Identifiant de connexion SFDC', 'config_comments'=>'Identifiant de connexion à salesforce', 'council_enabled'=>0],
            ['id' => 4, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'SFDC_PASSWORD', 'config_value'=>'#SFDC_PASSWORD#', 'config_marker'=>'SFDC', 'activated'=>1, 'council_activated'=>0, 'config_label'=>'Mot de passe de connexion à Salesforce', 'config_comments'=>'Mot de passe de connexion à Salesforce', 'council_enabled'=>0],
            ['id' => 5, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'SFDC_TOKEN', 'config_value'=>'#SFDC_TOKEN#', 'config_marker'=>'SFDC', 'activated'=>1, 'council_activated'=>0, 'config_label'=>'Token de sécurité SFDC', 'config_comments'=>'Jeton de sécurité pour la connexion à Salesforce', 'council_enabled'=>0],
            ['id' => 6, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'SFDC_WSDL', 'config_value'=>'#SFDC_WSDL#', 'config_marker'=>'SFDC', 'activated'=>1, 'council_activated'=>0, 'config_label'=>'Wsdl Salesforce', 'config_comments'=>'Activation du module règlement', 'council_enabled'=>0],
            ['id' => 11, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'MOD_AVATAR_ACTIVATED', 'config_value'=>'0', 'config_marker'=>'USER', 'activated'=>1, 'council_activated'=>1, 'config_label'=>'Module Règlement', 'config_comments'=>'Emplacement du WSDL permettant la connexion à Salesforce', 'council_enabled'=>1],
            ['id' => 12, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'council_id' => 1, 'config_key' => 'BASE_HREF', 'config_value'=>'#BASE_HREF#', 'config_marker'=>'PORTAL', 'activated'=>1, 'council_activated'=>1, 'config_label'=>'Avateur utilisateur', 'config_comments'=>"Activation de l''avateur utilisateur", 'council_enabled'=>0]
        );
 
        //// Uncomment the below to run the seeder
        DB::table('portal_configs')->insert($portal_configs);
    }
 
}
