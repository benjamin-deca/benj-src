<?php

use Illuminate\Database\Seeder;

class Open_hoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('open_hours')->delete();

        $open_hours = array([
        	'id' 				=> 1,
        	'created_at'		=> new DateTime,
        	'updated_at'		=> new DateTime,
        	'mondayAmOpen'		=> '09:00',
        	'mondayAmClose'		=> '12:00',
        	'mondayPmOpen'		=> '14:00',
        	'mondayPmClose'		=> '17:00',
			'tuesdayAmOpen'		=> '09:00',
			'tuesdayAmClose'	=> '12:00',
			'tuesdayPmOpen'		=> NULL,
			'tuesdayPmClose'	=> NULL,
			'wednesdayAmOpen'	=> '09:30',
			'wednesdayAmClose'	=> '12:30',
			'wednesdayPmOpen'	=> '14:30',
			'wednesdayPmClose'	=> '17:30',
			'thursdayAmOpen'	=> '09:00',
			'thursdayAmClose'	=> '12:00',
			'thursdayPmOpen'	=> NULL,
			'thursdayPmClose'	=> NULL,
			'fridayAmOpen'		=> '09:00',
			'fridayAmClose'		=> '12:00',
			'fridayPmOpen'		=> '14:00',
			'fridayPmClose'		=> '17:00',
			'saturdayAmOpen'	=> '10:00',
			'saturdayAmClose'	=> '14:00',
			'saturdayPmOpen'	=> NULL,
			'saturdayPmClose'	=> NULL,
			'sundayAmOpen'		=> NULL,
			'sundayAmClose'		=> NULL,
			'sundayPmOpen'		=> NULL,
			'sundayPmClose'		=> NULL
        ]);

        DB::table('open_hours')->insert($open_hours);
    }
}
