<?php
 
use Illuminate\Database\Seeder;
 
class CaseobjectsTableSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('casemotives')->delete();
        DB::table('caseobjects')->delete();
 
        $caseobjects = array(
            ['id' => 1, 'object' => 'Élection', 'externalcrm_object' => 'État civil', 'activated' => 1],
            ['id' => 2, 'object' => 'Environnement', 'externalcrm_object' => 'Infrastructures/voirie', 'activated' => 1],
            ['id' => 3, 'object' => 'État civil', 'externalcrm_object' => 'État civil', 'activated' => 1],
            ['id' => 4, 'object' => 'Loisirs', 'externalcrm_object' => ' ', 'activated' => 1],
            ['id' => 5, 'object' => 'Petite enfance', 'externalcrm_object' => 'Petite enfance', 'activated' => 1],
            ['id' => 6, 'object' => 'Scolaire', 'externalcrm_object' => 'Éducation', 'activated' => 1],
            ['id' => 7, 'object' => 'Sécurité', 'externalcrm_object' => ' ', 'activated' => 1],
            ['id' => 8, 'object' => 'Services techniques', 'externalcrm_object' => 'Infrastructures/voirie', 'activated' => 1],
            ['id' => 9, 'object' => 'Social', 'externalcrm_object' => 'CCAS', 'activated' => 1],
            ['id' => 10, 'object' => 'Voirie/Urbanisme', 'externalcrm_object' => 'Infrastructures/voirie', 'activated' => 1]
     

        );
 
        //// Uncomment the below to run the seeder
        DB::table('caseobjects')->insert($caseobjects);
    }
 
}
