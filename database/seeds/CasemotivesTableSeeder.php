<?php
 
use Illuminate\Database\Seeder;
 
class CasemotivesTableSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('casemotives')->delete();
 
        $casemotives = array(
            ['id' => 1, 'motive' => 'Acte de décès', 'externalcrm_motive' => "Demande d'acte de décès", 'object_id' => 3, 'activated' => 1],
            ['id' => 2, 'motive' => 'Acte de mariage', 'externalcrm_motive' => "Demande d'acte de mariage", 'object_id' => 3, 'activated' => 1],
            ['id' => 3, 'motive' => 'Acte de naissance', 'externalcrm_motive' => "Demande d'acte de naissance", 'object_id' => 9, 'activated' => 1],
            ['id' => 4, 'motive' => 'Aide ménagère', 'externalcrm_motive' => 'Aide ménagère', 'object_id' => 3, 'activated' => 1],
            ['id' => 5, 'motive' => 'Alignement, numérotage, raccordement', 'externalcrm_motive' => 'Alignement, numérotage, raccordement', 'object_id' => 10, 'activated' => 1],
            ['id' => 6, 'motive' => 'Bourse', 'externalcrm_motive' => 'Demande de bourse', 'object_id' => 6, 'activated' => 1],
            ['id' => 7, 'motive' => "Certificat d'alignement", 'externalcrm_motive' => "Certificat d'alignement", 'object_id' => 10, 'activated' => 1],
            ['id' => 8, 'motive' => 'Enlèvement des déchets encombrants', 'externalcrm_motive' => 'Enlèvement des déchets encombrants', 'object_id' => 2, 'activated' => 1],
            ['id' => 9, 'motive' => 'Enlèvement des déchets verts', 'externalcrm_motive' => 'Enlèvement des déchets verts', 'object_id' => 2, 'activated' => 1],
            ['id' => 10, 'motive' => "Inscription à l'accueil d'urgence", 'externalcrm_motive' => "Inscription à l'accueil d'urgence", 'object_id' => 5, 'activated' => 1],
            ['id' => 11, 'motive' => "Inscription à l'école de musique", 'externalcrm_motive' => '?', 'object_id' => 4, 'activated' => 1],
            ['id' => 12, 'motive' => 'Inscription à la bibliothèque', 'externalcrm_motive' => '?', 'object_id' => 4, 'activated' => 1],
            ['id' => 13, 'motive' => 'Inscription au centre de loisirs', 'externalcrm_motive' => 'Inscription extra-scolaire', 'object_id' => 6, 'activated' => 1],
            ['id' => 14, 'motive' => 'Inscription au centre de vacances', 'externalcrm_motive' => '?', 'object_id' => 4, 'activated' => 1],
            ['id' => 15, 'motive' => 'Inscription au restaurant scolaire', 'externalcrm_motive' => 'Inscription au restaurant scolaire', 'object_id' => 10, 'activated' => 1],
            ['id' => 16, 'motive' => 'Inscription aux activités périscolaires', 'externalcrm_motive' => 'Inscription périscolaire', 'object_id' => 6, 'activated' => 1],
            ['id' => 17, 'motive' => "Inscription aux ateliers d'éveil", 'externalcrm_motive' => "Inscription aux ateliers d'éveil", 'object_id' => 5, 'activated' => 1],
            ['id' => 18, 'motive' => 'Inscription en multi-accueil', 'externalcrm_motive' => 'Inscription en multi-accueil', 'object_id' => 5, 'activated' => 1],
            ['id' => 19, 'motive' => 'Inscription scolaire', 'externalcrm_motive' => 'Inscription scolaire', 'object_id' => 6, 'activated' => 1],
            ['id' => 20, 'motive' => 'Inscription sur les listes électorales', 'externalcrm_motive' => 'Inscription liste électorale', 'object_id' => 1, 'activated' => 1],
            ['id' => 21, 'motive' => 'Intervention technique', 'externalcrm_motive' => 'Intervention technique', 'object_id' => 8, 'activated' => 1],
            ['id' => 22, 'motive' => 'Intervention technique', 'externalcrm_motive' => 'Intervention technique', 'object_id' => 2, 'activated' => 1],
            ['id' => 23, 'motive' => 'Prestation de compensation du handicap (PCH) Adulte', 'externalcrm_motive' => 'Prestation de compensation du handicap (PCH) Adulte', 'object_id' => 9, 'activated' => 1],
            ['id' => 24, 'motive' => 'Prestation de compensation du handicap (PCH) Enfant de -20 ans', 'externalcrm_motive' => 'Prestation de compensation du handicap (PCH) Enfant de -20 ans', 'object_id' => 9, 'activated' => 1],
            ['id' => 25, 'motive' => "Raccordement à l'égout", 'externalcrm_motive' => "Raccordement à l'égout", 'object_id' => 10, 'activated' => 1],
            ['id' => 26, 'motive' => 'Recensement militaire', 'externalcrm_motive' => 'Recensement militaire', 'object_id' => 3, 'activated' => 1],
            ['id' => 27, 'motive' => 'Sécurité vacances', 'externalcrm_motive' => '?', 'object_id' => 7, 'activated' => 1],
            ['id' => 28, 'motive' => 'Téléassitance', 'externalcrm_motive' => 'Téléassitance', 'object_id' => 9, 'activated' => 1]

        );
 
        //// Uncomment the below to run the seeder
        DB::table('casemotives')->insert($casemotives);
    }
 
}


