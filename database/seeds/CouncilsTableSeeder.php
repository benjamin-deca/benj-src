<?php
 
use Illuminate\Database\Seeder;
 
class CouncilsTableSeeder extends Seeder {
 
    public function run()
    {
        $this->call('Open_hoursTableSeeder');

        // Uncomment the below to wipe the table clean before populating
        DB::table('councils')->delete();
 
        $councils = array(
            ['id' => 1,
            'created_at' => new DateTime, 
            'updated_at' => new DateTime, 
            'fullname'=> 'Mairie de Gi&eacute;vres', 
            'address'=>'12 rue des fleurs',
            'city'=>'Gievres', 
            'zipcode'=>'41130', 
            'country'=>'France', 
            'email'=>'contact@gievres.fr', 
            'landlinephone'=>'0123456789', 
            'logo' => 'Logo_Mairie_Mini.png',
            'website'=>'gievres.fr', 
            'mayorName'=>'Test', 
            'mayorPhone'=>'0124534545', 
            'mayorMobilePhone'=>'054656546', 
            'mayorEmail'=>'johndoe@gievres.fr',
            'open_hours_id' => 1]
        );
 
        //// Uncomment the below to run the seeder
        DB::table('councils')->insert($councils);
    }
 
}
