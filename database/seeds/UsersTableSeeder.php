<?php
 
use Illuminate\Database\Seeder;
 
class UsersTableSeeder extends Seeder {
 
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('users')->delete();
 
        $users = array(
            ['id' => 1,  'council_id' => 1, 'email' => 'cedric_admin@queriadis.com',      'password' => '$2y$10$aCfS9OVjiTwIzC7zVcOY5.60PYcKz.WeA43c1jtg8yrHqGMxlb/2.', 'lastname' => 'mougne', 'firstname' => 'cedric', 'title' => 'M.', 'women_name' => '', 'engaged_status' => 'Célibataire', 'address' => '12 rue des templiers', 'zipcode' => '41130', 'city' => 'gievres', 'country' => 'FRA', 'landline_phone' => '0123456789', 'mobile_phone' => '0698765432', 'socialsecurity_id' => '', 'birthdate' => '2015-09-09', 'birthplace' => '', 'externalcrm_id' => '0032400000BGi4qAAD', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'remember_token' => 'iMNHk5EZbAV1bpkFzw8LdTift1coDjmK5c9SshjQPaGBw80xXLVO0KitHX3P', 'is_admin' => '1',  'is_admin_deca' => NULL],
            ['id' => 7,  'council_id' => 1, 'email' => 'cedric_admin_deca@queriadis.com', 'password' => '$2y$10$aCfS9OVjiTwIzC7zVcOY5.60PYcKz.WeA43c1jtg8yrHqGMxlb/2.', 'lastname' => 'mougne', 'firstname' => 'cedric', 'title' => 'M.', 'women_name' => '', 'engaged_status' => 'Célibataire', 'address' => '12 rue des templiers', 'zipcode' => '41130', 'city' => 'gievres', 'country' => 'FRA', 'landline_phone' => '0123456789', 'mobile_phone' => '0698765432', 'socialsecurity_id' => '', 'birthdate' => '2015-09-09', 'birthplace' => '', 'externalcrm_id' => '0032400000BGi4qAAD', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'remember_token' => 'iMNHk5EZbAV1bpkFzw8LdTift1coDjmK5c9SshjQPaGBw80xXLVO0KitHX3P', 'is_admin' => '1',  'is_admin_deca' => NULL],
            ['id' => 10, 'council_id' => 1, 'email' => 'cedric@queriadis.com',            'password' => '$2y$10$aCfS9OVjiTwIzC7zVcOY5.60PYcKz.WeA43c1jtg8yrHqGMxlb/2.', 'lastname' => 'mougne', 'firstname' => 'cedric', 'title' => 'M.', 'women_name' => '', 'engaged_status' => 'Célibataire', 'address' => '12 rue des templiers', 'zipcode' => '41130', 'city' => 'gievres', 'country' => 'FRA', 'landline_phone' => '0123456789', 'mobile_phone' => '0698765432', 'socialsecurity_id' => '', 'birthdate' => '2015-09-09', 'birthplace' => '', 'externalcrm_id' => '0032400000BGi4qAAD', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'remember_token' => 'nUISdl3E7awLl1xp596cLs61lqQzjkzngmFRVzxs5rG7RbgSjTy35AHzkoBb', 'is_admin' => NULL, 'is_admin_deca' => NULL]

        );
 
        //// Uncomment the below to run the seeder
        DB::table('users')->insert($users);
    }
 
}

