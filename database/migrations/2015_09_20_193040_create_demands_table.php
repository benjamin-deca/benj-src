<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demands', function(Blueprint $table) {
			$table->increments('id');
            $table->timestamps();
			$table->integer('portal_id')->unsigned();
            $table->string('contact_id', 255);
			$table->string('external_id', 255);
			$table->string('object_id', 255);
			$table->string('objectLbl', 255);
			$table->string('motive_id', 255);
			$table->string('motiveLbl', 255);
			$table->string('comments', 255);
			$table->string('created_date', 255);
			$table->string('closed_date', 255);
			$table->string('priority', 255);
			$table->string('contact_medium', 255);
			$table->string('status', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('demands');
    }
}
