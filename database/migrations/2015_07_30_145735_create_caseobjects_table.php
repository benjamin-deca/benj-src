<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseobjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caseobjects', function($table)
		{
			$table->increments('id');
			$table->string('object',255);
			$table->string('externalcrm_object',255);
			$table->boolean('activated');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('caseobjects');
    }
}
