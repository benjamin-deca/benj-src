<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FromPortalIdToCouncilId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('councils', function($table) {
            $table->dropColumn('portal_id');
        });

        Schema::table('demands', function($table) {
            $table->renameColumn('portal_id', 'council_id');
        });

        Schema::table('portal_configs', function($table) {
            $table->renameColumn('portal_id', 'council_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('councils', function($table) {
            $table->Integer('portal_id');
        });

        Schema::table('demands', function($table) {
            $table->renameColumn('council_id', 'portal_id');
        });

        Schema::table('portal_configs', function($table) {
            $table->renameColumn('council_id', 'portal_id');
        });
    }
}
