<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouncilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('councils', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->integer('portal_id')->unsigned();
			$table->string('fullname',255);
			$table->string('address',255);
			$table->string('city',255);
			$table->string('zipcode',15);
			$table->string('country',50);
			$table->string('email',255);
			$table->string('landlinephone',15);
			$table->string('logo',100);
			$table->string('website',255);
			$table->string('openMonday',255);
			$table->string('openTuesday',255);
			$table->string('openWednesday',255);
			$table->string('openThursday',255);
			$table->string('openFriday',255);
			$table->string('openSaturday',255);
			$table->string('openSunday',255);
			$table->string('mayorName',255);
			$table->string('mayorPhone',15);
			$table->string('mayorMobilePhone',15);
			$table->string('mayorEmail',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('councils');
    }
}
