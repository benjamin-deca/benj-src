<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_hours', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->time('mondayAmOpen')->nullable();;
            $table->time('mondayAmClose')->nullable();;
            $table->time('mondayPmOpen')->nullable();;
            $table->time('mondayPmClose')->nullable();;
            $table->time('tuesdayAmOpen')->nullable();;
            $table->time('tuesdayAmClose')->nullable();;
            $table->time('tuesdayPmOpen')->nullable();;
            $table->time('tuesdayPmClose')->nullable();;
            $table->time('wednesdayAmOpen')->nullable();;
            $table->time('wednesdayAmClose')->nullable();;
            $table->time('wednesdayPmOpen')->nullable();;
            $table->time('wednesdayPmClose')->nullable();;
            $table->time('thursdayAmOpen')->nullable();;
            $table->time('thursdayAmClose')->nullable();;
            $table->time('thursdayPmOpen')->nullable();;
            $table->time('thursdayPmClose')->nullable();;
            $table->time('fridayAmOpen')->nullable();;
            $table->time('fridayAmClose')->nullable();;
            $table->time('fridayPmOpen')->nullable();;
            $table->time('fridayPmClose')->nullable();;
            $table->time('saturdayAmOpen')->nullable();;
            $table->time('saturdayAmClose')->nullable();;
            $table->time('saturdayPmOpen')->nullable();;
            $table->time('saturdayPmClose')->nullable();;
            $table->time('sundayAmOpen')->nullable();;
            $table->time('sundayAmClose')->nullable();;
            $table->time('sundayPmOpen')->nullable();;
            $table->time('sundayPmClose')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('open_hours');
    }
}
