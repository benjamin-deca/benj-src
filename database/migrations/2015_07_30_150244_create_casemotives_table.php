<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasemotivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casemotives', function ($table) 
		{
            $table->increments('id');
			$table->string('motive',255);
			$table->string('externalcrm_motive',255);
			$table->integer('object_id')->unsigned();
			$table->boolean('activated');
			$table->foreign('object_id')->references('id')->on('caseobjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('casemotives');
    }
}
