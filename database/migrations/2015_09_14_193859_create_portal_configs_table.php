<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->integer('portal_id')->unsigned();
			$table->string('config_key',255);
			$table->string('config_value',255);
			$table->string('config_marker',255);
			$table->boolean('activated');
			$table->boolean('council_activated');
            $table->string('config_label',255);
            $table->text('config_comments');
			$table->boolean('council_enabled');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portal_configs');
    }
}
