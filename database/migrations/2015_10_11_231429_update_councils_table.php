<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCouncilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('councils', function($table)
        {
            $table->dropColumn('openMonday');
            $table->dropColumn('openTuesday');
            $table->dropColumn('openWednesday');
            $table->dropColumn('openThursday');
            $table->dropColumn('openFriday');
            $table->dropColumn('openSaturday');
            $table->dropColumn('openSunday');
            $table->integer('open_hours_id')->unsigned();
            $table->foreign('open_hours_id')->references('id')->on('open_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('councils', function($table)
        {
            $table->string('openMonday', 255);
            $table->string('openTuesday', 255);
            $table->string('openWednesday', 255);
            $table->string('openThursday', 255);
            $table->string('openFriday', 255);
            $table->string('openSaturday', 255);
            $table->string('openSunday', 255);
            $table->dropForeign('councils_open_hours_id_foreign');
            $table->dropColumn('open_hours_id');
        });
    }
}
