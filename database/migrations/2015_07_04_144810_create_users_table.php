<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('email',255)->unique();
			$table->string('password',255);
			$table->string('lastname',255);
			$table->string('firstname',255);
			$table->string('title',5);
			$table->string('women_name',255);
			$table->string('engaged_status',255);
			$table->string('address',255);
			$table->string('zipcode',10);
			$table->string('city',255);
			$table->string('country',3);
			$table->string('landline_phone',20);
			$table->string('mobile_phone',20);
			$table->string('socialsecurity_id',20);
			$table->date('birthdate');
			$table->string('birthplace',255);
			$table->string('externalcrm_id',255);
			$table->timestamps('');
			$table->string('remember_token',255);
			$table->boolean('is_admin')->nullable();
			$table->boolean('is_admin_deca')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}