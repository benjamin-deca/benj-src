<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseMotive extends Model
{
	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'casemotives';

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['motive','externalcrm_motive','object_id','activated'];
}
