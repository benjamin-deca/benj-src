<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication form
Route::get('auth/login','Auth\AuthController@getLogin');
Route::get('auth/reset','Auth\AuthController@getLoginAfterResetPassword');
Route::post('auth/login','Auth\AuthController@postLogin');
Route::get('auth/logout','Auth\AuthController@getLogout');
//

// Registration form
Route::get('auth/register','Auth\AuthController@getRegister');
Route::post('auth/register','Auth\AuthController@postRegister');
//
// Password reset link request
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
//
// Password reset
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
Route::post('password/reset/{token}', 'Auth\PasswordController@postReset');

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function() {

	Route::get('home', function() {
		return Redirect::route('demands.index');
	});
	
	// DEBUT Route utilisateurs 
	Route::model('users', 'App\User');
	Route::resource('users', 'UserController');

	Route::bind('users', function($value, $route) {
		return App\User::whereEmail($value)->first();
	});
	
	Route::get('modifyPassword', 'UserController@modifyPassword');
	Route::match(['get', 'post'],'updatePwd', function() {
		return view('users.partials.updatePwd');
	});
	// FIN Route utilisateurs
	
	// DEBUT Routes Mairie
	Route::get('council', function() {
    	return View::make('councils.show')->with('council', Session::get('council'));
	});
	
	Route::model('councils', 'App\Council');
	Route::resource('councils', 'CouncilsController');
	// FIN routes mairie
	
	Route::get('account', function() {
		return view('account');
	});
	
	Route::get('modifyAccount', 'UserController@modifyAccount');
	
	Route::get('addLinkFoyer', 'UserController@addLinkFoyer');
	Route::get('deleteLinkFoyer', 'UserController@deleteLinkFoyer');
	
	Route::get('getAssociation', 'UserController@getAssociation');
	Route::get('addLinkAssociation', 'UserController@addLinkAssociation');
	Route::get('deleteLinkAssociation', 'UserController@deleteLinkAssociation');
	
	Route::match(['get', 'post'],'modifyCoord', function() {
		return view('modifyCoord');
	});
	Route::match(['get', 'post'],'modifyAddress', function() {
		return view('modifyAddress');
	});
	
	Route::match(['get', 'post'],'addLinkFoy', function() {
		return view('addLinkFoy');
	});
	Route::match(['get', 'post'],'addLinkAsso', function() {
		return view('addLinkAsso');
	});
	
	Route::get('editHouseMember', function() {
		return view('editHouseMember');
	});
	
	Route::get('editAssociation', function() {
		return view('editAssociation');
	});

	// DEBUT Route des demandes
	Route::model('demands', 'App\Demand');
	Route::resource('demands', 'DemandsController');
	Route::bind('demands', function($value, $route) {
		return App\Demand::whereExternalId($value)->first();
	});
	
	// Liste des motifs lies a un objet
	Route::get('getMotifsFromObject/{id}','DemandsController@getMotifsFromObject');
	
	// Ajout d'un commentaire sur une demande
	Route::get('submitComment', 'DemandsController@submitComment');
	Route::match(['get', 'post'],'addComment', function() {
		return view('demands.partials.addComment');
	});
	// FIN Route des demandes
	
	// Route des evenements
	Route::get('events', function() {
		return view('events');
	});
	
	//Admin
	Route::get('admin', function() {
		return view('admin');
	});

	Route::get('admin2', function() {
		return view('admin2');
	});	

	Route::get('admin3', function() {
		return view('admin2');
	});	

	Route::get('admin4', function() {
		return view('admin2');
	});	

	Route::get('admin5', function() {
		return view('admin2');
	});	
	Route::get('admin6', function() {
		return view('admin2');
	});	
	Route::group(['middleware' => 'admin'], function() {
		Route::model('portalConfigs', 'App\PortalConfig');
		Route::resource('portalConfigs', 'PortalConfigsController');
	});//aaa
	//bbbb
	//cccc
});