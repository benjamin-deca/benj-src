<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB as DB;
use Illuminate\Http\Request;
use Response;

use Davispeixoto\Laravel5Salesforce\Salesforce as Salesforce;
use Illuminate\Config\Repository as Repository;
use Illuminate\Translation\FileLoader;
use Mail;

use App\User as User;

use \stdClass;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Redirect;
use Form;

class UserController extends Controller {

	protected $rules = [
		'title' => ['required'],		
		'lastname' => ['required'],	
		'firstname' => ['required'],	
		'email' => ['required'],
	];

	/* Méthode qui met à jour les données de l'utilisateur */
	public function modifyAccount(Request $request) {
		
		$this->validate($request, [
			'landlinephone' => 'numeric|digits:10',
			'mobilephone' => 'numeric|digits:10',
			'email' => 'email|max:255|unique:users',
			'zipcode' => 'numeric|digits:5',
			'address' => 'max:255',
			'city' => 'max:100',
		]);
		
		$configSFDC = session('sfdcConfig');
		$environment = 'local';
		$id = session('userExternalId');
		$config = new Repository($configSFDC, $environment);
		$mySFDC = new Salesforce($config);
		
		$updateDB = array();
		$records = array();
		$records[0] = new stdclass();
		$records[0]->Id = $id;
		$param = false;
		
		if($request->has('sit')) {
			$updateDB += array('engaged_status' => $request->input('sit'));
			$records[0]->Situation_maritale__c = $request->input('sit');
			$param = true;
		}
		if($request->has('bir')) {
			$birthdate_format = date_create_from_format('d/m/Y', $request->input('bir'));
			$birthdate = date_format($birthdate_format, 'Y-m-d');
			$updateDB += array('birthdate' => $birthdate);
			$records[0]->Birthdate = $birthdate;
			$param = true;
		}
		if($request->has('landlinephone')) {
			$updateDB += array('landline_phone' => $request->input('landlinephone'));
			$records[0]->Phone = $request->input('landlinephone');
			$param = true;
		}
		if($request->has('mobilephone')) {
			$updateDB += array('mobile_phone' => $request->input('mobilephone'));
			$records[0]->MobilePhone = $request->input('mobilephone');
			$param = true;
		}
		if($request->has('email')) {
			$updateDB += array('email' => $request->input('email'));
			$records[0]->Email = $request->input('email');
			$param = true;
		}
		if($request->has('address')) {
			$updateDB += array('address' => $request->input('address'));
			$records[0]->Adresse__c = $request->input('address');
			$param = true;
		}
		if($request->has('zipcode')) {
			$updateDB += array('zipcode' => $request->input('zipcode'));
			$records[0]->Code_Postal__c = $request->input('zipcode');
			$param = true;
		}
		if($request->has('city')) {
			$updateDB += array('city' => $request->input('city'));
			$records[0]->Ville__c = $request->input('city');
			$param = true;
		}
		if($request->has('cou')) {
			$updateDB += array('country' => $request->input('cou'));
			$records[0]->Pays__c = $request->input('cou');
			$param = true;
		}
		
		if($param) {
			DB::table('users')->where('externalcrm_id','=',$id)->update($updateDB);
			$mySFDC->sfh->update($records, 'Contact');
		}
		
		return $request->json()->all();
	}
	
	/* Méthode qui modifie le mot de passe de l'utilisateur */
	public function modifyPassword(Request $request) {
		$this->validate($request, [
			'password' => 'required|confirmed|min:8'
		]);
		
		$id = session('userExternalId');
		
		if($request->has('password')) {
			$updateDB = array('password' => bcrypt($request->input('password')));
			DB::table('users')->where('externalcrm_id','=',$id)->update($updateDB);
			
			$email = DB::table('users')->where('externalcrm_id','=',$id)->value('email');
			
			// On envoie un mail
			Mail::send('emails.newpassword', [], function($message) use($email) {
				$message->from(config('mail.from.address'), config('mail.from.name'));
				$message->to($email);
				$message->subject('Réinitialisation de votre mot de passe');
			});	
		}
		return $request->json()->all();
		
	}
	
	/* Méthode qui ajoute un lien entre deux personnes du foyer */
	public function addLinkFoyer(Request $request) {
		
		$this->validate($request, [
			'lastname' => 'required',
			'firstname' => 'required',
			'birthdate' => 'required',
			'typelink' => 'required',
		]);
		
		$configSFDC = session('sfdcConfig');
		$environment = 'local';
		$customerId = session('userExternalId');
		$config = new Repository($configSFDC, $environment);
		$mySFDC = new Salesforce($config);
		
		$foyerId = ' ';
		
		// On remet la date de naissance à son format Salesforce
		$birthdate_format = date_create_from_format('d/m/Y', $request->input('birthdate'));
		$birthdate = date_format($birthdate_format, 'Y-m-d');
		
		// Première étape : on vérifie si l'utilisateur qu'on souhaite ajouter est présent ou pas sur Salesforce
		$accountQuery = "SELECT Id from Contact where FirstName = '" . $request->input('firstname') . "' AND LastName = '" . $request->input('lastname') . "' AND Birthdate = " . $birthdate . "";
		$accountResponse = $mySFDC->sfh->query($accountQuery);
		
		$records = array();
		$records[0] = new stdClass();
		$records[0]->FirstName = $request->input('firstname');
		$records[0]->LastName = $request->input('lastname');
		$records[0]->Birthdate = $birthdate;
		if($request->has('title')) {
			$records[0]->Salutation = $request->input('title');
		}
		if($request->has('birthplace')) {
			$records[0]->Lieu_de_naissance__c = $request->input('birthplace');
		}
		
		if(count($accountResponse->records) == 0) {
			// L'utilisateur n'est pas présent sur Salesforce, on l'ajoute
			$response = $mySFDC->sfh->create($records,'Contact');
			$foyerId = $response[0]->id;
		} else {
			// Le compte existe et on le met à jour avec les données que l'utilisateur a rempli
			$foyerId = $accountResponse->records[0]->Id;
			$records[0]->Id = $foyerId;
			$response = $mySFDC->sfh->update($records,'Contact');
		}
		
		// Seconde étape : on ajoute le lien "Salesforce" entre les deux utilisateurs
		$records_link = array();
		$records_link[0] = new stdClass();
		$records_link[0]->Personne__c = $customerId;
		$records_link[0]->Personne_liee__c = $foyerId;
		$records_link[0]->Foyer__c = true;
		$records_link[0]->Type_de_lien__c = $request->input('typelink');
		
		// Le lien dans l'autre sens est automatiquement créé via un trigger sur Salesforce
		$mySFDC->sfh->create($records_link, 'Lien__c');
		
		return $request->json()->all();
	}
	
	/* Méthode qui supprime un lien entre deux personnes du foyer */
	public function deleteLinkFoyer(Request $request) {
		
		$configSFDC = session('sfdcConfig');
		$environment = 'local';
		$customerId = session('userExternalId');
		$config = new Repository($configSFDC, $environment);
		$mySFDC = new Salesforce($config);

		$contactId = $request->input('idContact');
		
		// Il faut supprimer les deux objets "Lien__c" sur Salesforce car il n'y a pas de trigger de suppression sur Salesforce des liens automatiques
		$linkQuery = "SELECT Id from Lien__c where Personne__r.Id = '" . $customerId . "' AND Personne_liee__r.Id = '" . $contactId . "'";
		$linkResponse = $mySFDC->sfh->query($linkQuery);
		$arrayContactId = array();
		array_push($arrayContactId, $linkResponse->records[0]->Id);
		$mySFDC->sfh->delete($arrayContactId);

		return $request->json()->all();
		
	}
	
	/* Méthode qui récupère la liste des associations présentes sur Salesforce */
	public function getAssociation() {
		
		$configSFDC = session('sfdcConfig');
		$environment = 'local';
		$customerId = session('userExternalId');
		$config = new Repository($configSFDC, $environment);
		$mySFDC = new Salesforce($config);
		
		$assoQuery = "SELECT Id, Name FROM Association__c ORDER BY Name";
		$assoResponse = $mySFDC->sfh->query($assoQuery);
		
		$options = array();
		foreach($assoResponse->records as $record) {
			$options += array($record->Id => $record->Name);
		}
		return Response::json($options);
	}
	
	/* Méthode qui ajoute un nouveau lien entre l'association et la personne */
	public function addLinkAssociation(Request $request) {
		
		$this->validate($request, [
			'id_asso' => 'required',
			'fonction' => 'required',
		]);
		
		$configSFDC = session('sfdcConfig');
		$environment = 'local';
		$id = session('userExternalId');
		$config = new Repository($configSFDC, $environment);
		$mySFDC = new Salesforce($config);
		
		$records = array();
		$records[0] = new stdClass();
		$records[0]->Association__c = $request->input('id_asso');
		$records[0]->Personne__c = $id;
		$records[0]->Fonction__c = $request->input('fonction');
		
		if($request->has('since_asso')) {
			$since_asso_format = date_create_from_format('d/m/Y', $request->input('since_asso'));
			$since_asso = date_format($since_asso_format, 'Y-m-d');
			$records[0]->Depuis_le__c = $since_asso;
		}
		$mySFDC->sfh->create($records, 'Lien_Association_Personne__c');
		
		return $request->json()->all();
	}
	
	/* Méthode qui supprime un lien entre l'association et la personne */
	public function deleteLinkAssociation(Request $request) {
		
		$configSFDC = session('sfdcConfig');
		$environment = 'local';
		$customerId = session('userExternalId');
		$config = new Repository($configSFDC, $environment);
		$mySFDC = new Salesforce($config);
		
		$assoId = $request->input('idAsso');
		
		$linkQuery = "SELECT Id from Lien_Association_Personne__c where Personne__r.Id = '" . $customerId . "' AND Association__r.Id = '" . $assoId . "'";
		$linkResponse = $mySFDC->sfh->query($linkQuery);
		$arrayAssoId = array();
		array_push($arrayAssoId, $linkResponse->records[0]->Id);
		$mySFDC->sfh->delete($arrayAssoId);
		
		return $request->json()->all();
	}

		/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		//TODO ne lister ques les utilisateurs du meme council_id pour les admin non deca
		$users = User::all();
		return view('users.index', compact('users'));
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
		$councils = DB::table('councils')->get();
		return view('users.create', compact('councils'));
    }

    /**
     * Store a newly created resource in storage.
     *
	 * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
		
		$input = Input::all();
		//Specifique pour les checkbox
		$input['is_admin'] = (Input::has('is_admin')) ? (($input['is_admin'] == 'on') ? 1 : 0) : 0;
		$input['is_admin_deca'] = (Input::has('is_admin_deca')) ? (($input['is_admin_deca'] == 'on') ? 1 : 0) : 0;

		User::create($input);
 
		return Redirect::route('users.index')->with('message', 'Utilisateur crée');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $User
     * @return Response
     */
    public function show(User $User)
    {
        return view('users.show', compact('User'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $User
     * @return Response
     */
    public function edit(User $User)
    {
        $councils = DB::table('councils')->get();
		return view('users.edit', compact('User', 'councils'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param 	User $User
	 * @param 	Request $request
     * @return 	Response
     */
    public function update(User $User, Request $request)
    {
        $this->validate($request, $this->rules);
		
		$input = array_except(Input::all(), '_method');
		//Specifique pour les checkbox
		$input['is_admin'] = (Input::has('is_admin')) ? (($input['is_admin'] == 'on') ? 1 : 0) : 0;
		$input['is_admin_deca'] = (Input::has('is_admin_deca')) ? (($input['is_admin_deca'] == 'on') ? 1 : 0) : 0;
		$User->update($input);
 
		return Redirect::route('users.index', $User->id)->with('message', 'Compte utilisateur mis à jour.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $User
     * @return Response
     */
    public function destroy(User $User)
    {
        $User->delete();

		return Redirect::route('users.index')->with('message', 'Compte utilisateur supprimé');
    }
}
?>