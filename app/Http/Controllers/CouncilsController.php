<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Council as Council;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Redirect;
use Form;

class CouncilsController extends Controller
{
    protected $rules = [		
		'fullname' => ['required', 'max:50'],
        'landlinephone' => ['numeric', 'digits:10'],
        'email' => ['email', 'max:255'],
        'website' => ['active_url', 'max:50'],
		'address' => ['required', 'max:100'],	
		'zipcode' => ['required', 'digits:5'],	
		'city' => ['required', 'max:50'],	
		'country' => ['required', 'max:50'],
		'mayorName' => ['max:50'],
		'mayorPhone' => ['numeric', 'digits:10'],
		'mayorMobilePhone' => ['numeric', 'digits:10'],
        'mayorEmail' => ['email', 'max:50'],
	];
	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		//TODO Rajouter un filtre pour ne lister que :
		// La mairie de rattachement si admin
		// Toutes les mairies si admin DECA
		$councils = Council::all();
        return view('councils.index', compact('councils'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('councils.create');
    }

    /**
     * Store a newly created resource in storage.
     *
	 * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		$this->validate($request, $this->rules);       
		
		$input = Input::all();
		Council::create($input);
 
		return Redirect::route('councils.index')->with('message', 'Mairie cr��e');
    }

    /**
     * Display the specified resource.
     *
     * @param  Council $council
     * @return Response
     */
    public function show(Council $council)
    {
        return view('councils.show', compact('council'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Council $council
     * @return Response
     */
    public function edit(Council $council)
    {
        return view('councils.edit', compact('council'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  	Council $council
	 * @param 	Request $request
     * @return 	Response
     */
    public function update(Council $council, Request $request)
    {
        $this->validate($request, $this->rules);
		$input = array_except(Input::all(), '_method');
    	$council->update($input);

        foreach ($input as $k => $v) 
        {
            if (empty($v))
            {
                $input[$k] = null;
            }
        }
        
        $council->openHours()->update($input);

        session(['council' => $council]);   

		return Redirect::route('councils.index', $council->id)->with('message', 'Mairie mise � jour.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Council $council
     * @return Response
     */
    public function destroy(Council $council)
    {
        $council->delete();

		return Redirect::route('councils.index')->with('message', 'Mairie supprim�e');
    }
}
