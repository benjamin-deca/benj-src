<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Demand as Demand;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Davispeixoto\Laravel5Salesforce\Salesforce as Salesforce;
use Illuminate\Config\Repository as Repository;
use Illuminate\Translation\FileLoader;
use Illuminate\Support\Facades\DB as DB;

use Input;
use Redirect;
use Form;
use Response;

class DemandsController extends Controller
{
    protected $rules = [		
		'contact_id' => ['required'],	
		'status' => ['required'],	
		'contact_medium' => ['required'],
		'object_id' => ['required'],
		'motive_id' => ['required'],
		'comments' => ['required'],	
		'testsup'	=> ['required'],	
	];

	/**
     * Display a listing of motives from an object
     * @param	int $id
     * @return 	Response
     */
	public function getMotifsFromObject($id) {
		$motifs = DB::table('casemotives')->where('object_id','=',$id)->where('activated','=',1)->get();
		$options = array();
		
		foreach($motifs as $mot) {
			$options += array($mot->id => $mot->motive);
		}
		return Response::json($options);
	}
	
	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		// Visualiser les demandes en base
		//$Demands = Demand::all();

		$Demands = array();
		try {
			// La configuration Salesforce
			$configSFDC = session('sfdcConfig');
			$configSFDC2 = session('sfdcConfig');
			//specify the environment to load
			$environment = 'local';
			$environment2 = 'local';
			$environment3 = 'local';
			$environment4 = 'local';
			$environment5 = 'local';
			//create a new config object
			$config = new Repository($configSFDC, $environment);
			
			$mySFDC = new Salesforce($config);
			$customerId = session('userExternalId');
			
			//Recuperation des demandes de la personne
			$casesQuery = "SELECT Id, Name, RecordTypeId, CreatedDate, Moyen_de_contact__c, Motif_1__c, Motif_2__c, Statut__c, Date_de_cloture__c from Demande__c where Personne__r.Id = '" . $customerId . "'";
			$casesResult = $mySFDC->sfh->query($casesQuery);
			foreach($casesResult->records as $record) {
				$demand = new Demand([
					'council_id' => '1',
					'contact_id' => $customerId,
					'external_id' => $record->Id,
					'object_id' => '',
					'objectLbl' => (isset($record->Motif_1__c))? $record->Motif_1__c : '',
					'motive_id' => '',
					'motiveLbl' => (isset($record->Motif_2__c))? $record->Motif_2__c : '',
					'comments' => '',
					'status' => (isset($record->Statut__c))? $record->Statut__c : '',
					'created_date' => $record->CreatedDate,
					'closed_date' => (isset($record->Date_de_cloture__c))? $record->Date_de_cloture__c : '',
					'priority' => '',
					'contact_medium' => (isset($record->Moyen_de_contact__c))? $record->Moyen_de_contact__c : '']);
				$Demands[] = $demand;
			}
		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
		return view('demands.index', compact('Demands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $caseObjects = DB::table('caseobjects')->where('activated','=',1)->get();
		
		try {
			//La configuration Salesforce
			$configSFDC = session('sfdcConfig');

			//specify the environment to load
			$environment = 'local';
			//create a new config object
			$config = new Repository($configSFDC, $environment);
				
			$mySFDC = new Salesforce($config);
			$customerId = session('userExternalId');
			
			// On r�cup�re les donn�es de l'utilisateur
			$account = $mySFDC->sfh->retrieve('Id, Salutation, FirstName, LastName, Email, Phone, MobilePhone, Birthdate, Adresse__c, Code_Postal__c, Ville__c, Pays__c, Situation_maritale__c','Contact',array($customerId));

		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
		return view('demands.create', compact('caseObjects','account'));
    }

    /**
     * Store a newly created resource in storage.
     *
	 * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //$this->validate($request, $this->rules);
		$input = Input::all();
		$input['smsNotification'] = (Input::has('smsNotification')) ? (($input['smsNotification'] == 'on') ? 1 : 0) : 0;
		
		if ($input['smsNotification'] == '1') {
			//TODO Rajouter l'envoi du SMS
		}
		
		try {
			//La configuration Salesforce
			$configSFDC = session('sfdcConfig');
			//specify the environment to load
			$environment = 'local';
			//create a new config object
			$config = new Repository($configSFDC, $environment);
			$mySforceConnection = new Salesforce($config);
			
			$object_id = Input::get('object_id');
			$externalObject_id = DB::select('select externalcrm_object from caseobjects where id = ?', [$object_id]);
			$motive_id = Input::get('motive_id');
			$externalMotive_id = DB::select('select externalcrm_motive from casemotives where id = ?', [$motive_id]);
			
			
			$records = array();
			$records[0] = new \stdClass();
			$records[0]->Personne__c = $_POST['contact_id']; 						// ID PRESONNE
			$records[0]->Moyen_de_contact__c = $_POST['contact_medium']; 			// MOYEN DE CONTACT
			$records[0]->Motif_1__c = $externalObject_id[0]->externalcrm_object;	// OBJET
			$records[0]->Motif_2__c = $externalMotive_id[0]->externalcrm_motive;	// MOTIF
			$records[0]->Statut__c = $_POST['status']; 								// STATUT
			$records[0]->Priorite__c = $_POST['priority']; 							// PRIORITE
			$records[0]->Commentaires__c = $_POST['comments']; 						// COMMENTAIRES
			$response = $mySforceConnection->create($records, 'Demande__c');
			//Recuperation de l'id de la demande
			$input['external_id'] = $response[0]->id;
			//Stockage en base
			Demand::create( $input );
		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
		return Redirect::route('demands.index')->with('message', 'Demande cr��e');
    }

    /**
     * Display the specified resource.
     *
     * @param  Demand $Demand
     * @return Response
     */
    public function show(Demand $Demand)
    {
		return view('demands.show', compact('Demand'));
    }

	/**
     * Show the form for editing the specified resource.
     *
     * @param  Demand $Demand
     * @return Response
     */
    public function edit(Demand $oDemand)
    {
		try {
			
			//La configuration Salesforce
			$configSFDC = session('sfdcConfig');

			//specify the environment to load
			$environment = 'local';
			//create a new config object
			$config = new Repository($configSFDC, $environment);
				
			$mySFDC = new Salesforce($config);
			$customerId = session('userExternalId');
			
			$demandQuery = "SELECT Id, Name, RecordTypeId, CreatedDate, Moyen_de_contact__c, Priorite__c, Motif_1__c, Motif_2__c, Commentaires__c, Statut__c, Date_de_cloture__c from Demande__c where id = '" . $oDemand->external_id . "'";
			$demandResult = $mySFDC->sfh->query($demandQuery);
			
			//TODO a remplacer par un truc plus propre
			foreach ($demandResult->records as $record) {
				$res = $record;
			}
			$Demand = new Demand([
				'contact_id' => $customerId,
				'external_id' => $oDemand->external_id,
				'object_id' => (isset($res->Motif_1__c))? $res->Motif_1__c : '',
				'objectLbl' => (isset($res->Motif_1__c))? $res->Motif_1__c : '',
				'motive_id' => (isset($res->Motif_2__c))? $res->Motif_2__c : '',
				'motiveLbl' => (isset($res->Motif_2__c))? $res->Motif_2__c : '',
				'comments' => (isset($res->Commentaires__c))? $res->Commentaires__c : '',
				'status' => (isset($res->Statut__c))? $res->Statut__c : '',
				'created_date' => (isset($res->CreatedDate))? $res->CreatedDate : '',
				'closed_date' => (isset($res->Date_de_cloture__c))? $res->Date_de_cloture__c : '',
				'priority' => (isset($res->Priorite__c))? $res->Priorite__c : '',
				'contact_medium' => (isset($res->Moyen_de_contact__c))? $res->Moyen_de_contact__c : '']);
				
			//TODO passer en retrieve
			$notesQuery = "Select Body, CreatedDate from Note where ParentId = '" . $oDemand->external_id . "' AND IsPrivate = false AND IsDeleted = false order by CreatedDate ASC";
			$Notes = $mySFDC->sfh->query($notesQuery);
			
			//TODO passer en retrieve
			//$attachmentsQuery = "SELECT Body, CreatedDate from Attachment where ParentId = '" . $oDemand->external_id . "' AND IsPrivate = 0 AND IsDeleted = 0 order by CreatedDate ASC";
			//$attachmentsResult = $mySFDC->sfh->query($attachmentsQuery);
			
		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
		return view('demands.edit', compact('Demand','Notes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param 	Demand $Demand
	 * @param 	Request $request
     * @return 	Response
     */
    public function update(Demand $Demand, Request $request)
    {
         $this->validate($request, $this->rules);
		
		$input = array_except(Input::all(), '_method');
		$Demand->update($input);
 
		return Redirect::route('demands.index', $Demand->id)->with('message', 'Demande mise � jour');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Demand $Demand
     * @return Response
     */
    public function destroy(Demand $Demand)
    {
       //TODO Supprimer de SFDC
	   $Demand->delete();

		return Redirect::route('demands.index')->with('message', 'Demande supprim�e');
    }
	
	/* M�thode qui ajoute un commentaire a la demande */
	public function submitComment(Request $request) {
		
		/*
		$this->validate($request, [
			'comment' => 'required'
		]);
		*/
		if($request->has('comment')) {
			
			try {
				//La configuration Salesforce
				$configSFDC = session('sfdcConfig');
				//specify the environment to load
				$environment = 'local';
				//create a new config object
				$config = new Repository($configSFDC, $environment);
				$mySforceConnection = new Salesforce($config);

				$records = array();
				$records[0] = new \stdClass();
				$records[0]->Body = $_POST['comment'];
				$records[0]->Title = 'Reponse citoyen';
				$records[0]->IsPrivate = 0;
				$records[0]->Parent = 'Demand';
				$records[0]->Parentid = $_POST['external_id'];
				$response = $mySforceConnection->create($records, 'Note');

			} catch (Exception $e) {
				echo $e->getMessage();
				echo $e->getTraceAsString();
			}
			/*			
			$email = DB::table('users')->where('externalcrm_id','=',$id)->value('email');
			
			// On envoie un mail
			Mail::send('emails.newpassword', [], function($message) use($email) {
				$message->from(config('mail.from.address'), config('mail.from.name'));
				$message->to($email);
				$message->subject('R�initialisation de votre mot de passe');
			});	
			*/
		}
		return $request->json()->all();
		
	}
	
}
