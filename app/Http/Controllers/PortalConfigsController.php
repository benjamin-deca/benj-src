<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PortalConfig as PortalConfig;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Redirect;
use Form;

class PortalConfigsController extends Controller
{
	protected $rules = [		
		'config_key' => ['required', 'min:3'],		
		'config_value' => ['required'],	
		'config_label' => ['required'],	
		'config_marker' => ['required'],	
	];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //TODO Ne lister que les variables activated=1
		//TODO Rajouter le filtre council_enabled=1 pour les utilisateurs en mairie
		$portalConfigs = PortalConfig::all();
		return view('portalConfigs.index', compact('portalConfigs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('portalConfigs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
	 * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
		
		$input = Input::all();
		PortalConfig::create( $input );
 
		return Redirect::route('portalConfigs.index')->with('message', 'Variable cr��e');
    }

    /**
     * Display the specified resource.
     *
     * @param  PortalConfig $portalConfig
     * @return Response
     */
    public function show(PortalConfig $portalConfig)
    {
        return view('portalConfigs.show', compact('portalConfig'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  PortalConfig $portalConfig
     * @return Response
     */
    public function edit(PortalConfig $portalConfig)
    {
        return view('portalConfigs.edit', compact('portalConfig'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  	PortalConfig $portalConfig
	 * @param 	Request $request
     * @return 	Response
     */
    public function update(PortalConfig $portalConfig, Request $request)
    {
        $this->validate($request, $this->rules);
		
		$input = array_except(Input::all(), '_method');
		$portalConfig->update($input);
 
		return Redirect::route('portalConfigs.index', $portalConfig->id)->with('message', 'Variable mise � jour.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PortalConfig $portalConfig
     * @return Response
     */
    public function destroy(PortalConfig $portalConfig)
    {
        $portalConfig->delete();

		return Redirect::route('portalConfigs.index')->with('message', 'Variable supprim�e');
    }
}
