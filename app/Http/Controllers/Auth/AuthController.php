<?php

namespace App\Http\Controllers\Auth;

use App\Council;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Repository;
use Illuminate\Translation\FileLoader;
use Davispeixoto\Laravel5Salesforce\Salesforce;
use \stdClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use Illuminate\Support\Facades\DB as DB;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;
	
	// Page de redirection par defaut apres la connexion
	protected $redirectPath = '/home';
	
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'lastname' => 'required|max:255',
			'firstname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:8',
			'landlinephone' => 'numeric|digits:10',
			'mobilephone' => 'numeric|digits:10',
			'zipcode' => 'numeric|digits:5',
			'address' => 'max:255',
			'city' => 'max:100',
			'acceptTerms' => 'accepted',
			'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
		$id_crm = ' ';
		
		try {
			$this->setSfdcSessionDatas();
			$id_crm = $this->getUpsertedIdContactCitizenAccount($data);
			$user= User::create([
        	'council_id' => '1',
			'title' => $data['title'],
            'lastname' => $data['lastname'],
			'firstname' => $data['firstname'],
            'email' => $data['email'],
			'address' => $data['address'], 
			'zipcode' => $data['zipcode'], 
			'city' => $data['city'],
            'password' => bcrypt($data['password']),
			'country' => $data['country'],
			'women_name' => '',
			'engaged_status' => '',
			'landline_phone' => $data['landlinephone'], 
			'mobile_phone' => $data['mobilephone'], 
			'socialsecurity_id' => '', 
			'birthdate' => '', 
			'birthplace' => '',
			'externalcrm_id' => $id_crm,
        	]);
			session(['user' => $user]);
			session(['userExternalId' => $id_crm]);
			
			// On envoie le mail de confirmation de la création du Compte
			Mail::send('emails.creationaccount', ['user' => $user], function($message) use($user) {
			$message->from(config('mail.from.address'), config('mail.from.name'));
			$message->to($user->email);
			$message->subject('Création de votre compte citoyen');
			});
		
		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
        return $user;
    }
	
	/**
	 * Get the failed login message.
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage()
	{
		return 'Votre login et/ou votre mot de passe est invalide.';
	}

	 /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
        	try {
			//Mise en session de l'ID externe de l'utilisateur 
			// TODO recuperer l'ID avec la requete ci-dessous
			$userEmail = $request->input('email');
			$userExternalId = DB::table('users')->where('email','=',$userEmail)->lists('externalcrm_id');
			(sizeof($userExternalId) > 0) ? session(['userExternalId' => $userExternalId[0]]): session(['userExternalId' => '0']);
			$user = DB::table('users')->where('email','=',$userEmail)->first();		
			$this->setSfdcSessionDatas();
			session(['user' => $user]);
			} catch (Exception $e) {
				echo $e->getMessage();
				echo $e->getTraceAsString();
			}
            return redirect()->intended($this->redirectPath());
        }

        return redirect($this->loginPath())
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => $this->getFailedLoginMessage(),
            ]);
    }
	/**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
   /* protected function getCredentials(Request $request)
    {
		try {
			//Mise en session de l'ID externe de l'utilisateur 
			// TODO recuperer l'ID avec la requete ci-dessous
			$userEmail = $request->input('email');
			$userExternalId = DB::table('users')->where('email','=',$userEmail)->lists('externalcrm_id');
			(sizeof($userExternalId) > 0) ? session(['userExternalId' => $userExternalId[0]]): session(['userExternalId' => '0']);
			$user = DB::table('users')->where('email','=',$userEmail)->first();		
			$this->setSfdcSessionDatas();
			session(['user' => $user]);
		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
        return $request->only('email', 'password');
    }*/

    protected function getLoginAfterResetPassword()
    {
    	return view('auth.login')->withMessage('Votre mot de passe a bien été réinitialisé.');
    }

    /**
     * Positionne les variables de sessions initiales nécessaires au bon fonctionnement de l'app. (layout.blade.php)
     * keys : sfdcConfig, portalConfig, council 
     * @param  User  $user
     */
    protected function setSfdcSessionDatas(){
			
			//Connexion a SFDC
			$sfdcConfigDB = DB::table('portal_configs')->where('config_marker','=','SFDC')->where('activated','=','1')->lists('config_value','config_key');
			$sfdcConfigTmp = array();
			foreach ($sfdcConfigDB as $config_key => $config_value) {
				$sfdcConfigTmp[$config_key] = $config_value;
			}
			$sfdcConfig = array(
				'salesforce.username' => $sfdcConfigTmp['SFDC_USERNAME'],
				'salesforce.password' => $sfdcConfigTmp['SFDC_PASSWORD'],
				'salesforce.token' => $sfdcConfigTmp['SFDC_TOKEN'],
				'salesforce.wsdl' => storage_path($sfdcConfigTmp['SFDC_WSDL'])
			);
			session(['sfdcConfig' => $sfdcConfig]);
			
			//Ajout des variables de session
			$portalConfigDB = DB::table('portal_configs')->where('activated','=',1)->lists('config_value','config_key');
			$sfdcConfigTmp = array();
			foreach ($portalConfigDB as $config_key => $config_value) {
				$portalConfig[$config_key] = $config_value;
			}
			session(['portalConfig' => $portalConfig]);

			$council = Council::where('id','=', 1)->first();
			$council->load('openHours');

			session(['council' => $council]);
    }

    protected function getUpsertedIdContactCitizenAccount(array $data){
    	$id_crm = '';
    	$environment = 'local';	
		$config = new Repository(session('sfdcConfig'), $environment);
		$mySFDC = new Salesforce($config);
		// On vérifie ici si l'utilisateur est déjà inscrit sur Salesforce ou pas.
		$accountQuery = "SELECT Id, Email from Contact where Email = '" . $data['email'] . "'";
		$accountResponse = $mySFDC->sfh->query($accountQuery);
		
		$records = array();
		$records[0] = new stdClass();
		$records[0]->Cityzen_account__c = true;
		$records[0]->FirstName = $data['firstname'];
		$records[0]->LastName = $data['lastname'];
		$records[0]->Salutation = $data['title'];
		$records[0]->Email = $data['email'];
		$records[0]->Pays__c = 'France';
		if (!empty($data['city']))
			$records[0]->Ville__c = $data['city'];
		if (!empty($data['address']))
			$records[0]->Adresse__c = $data['address'];
		if (!empty($data['zipcode']))
			$records[0]->Code_Postal__c = $data['zipcode'];
		if (!empty($data['landlinephone']))
			$records[0]->Phone = $data['landlinephone'];
		if (!empty($data['mobilephone']))
			$records[0]->MobilePhone = $data['mobilephone'];

		if(count($accountResponse->records) == 0) {
			// L'utilisateur n'est pas présent sur Salesforce, on l'ajoute
			$response = $mySFDC->sfh->create($records,'Contact');
			$id_crm = $response[0]->id;
			
		} else {
			// Le compte existe et on le met à jour avec les données que l'utilisateur a rempli
			$id_crm = $accountResponse->records[0]->Id;
			$records[0]->Id = $id_crm;
			$response = $mySFDC->sfh->update($records,'Contact');
		}
		return $id_crm;
    }
}
