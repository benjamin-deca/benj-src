<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class portalConfig extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'portal_configs';

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['council_id','config_key','config_label','config_value','config_marker','config_comments','activated','council_enabled','council_activated'];

	/**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
	
	protected $guarded = [];
}