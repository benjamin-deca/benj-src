<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demand extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'demands';

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['council_id', 'contact_id', 'external_id', 'object_id', 'objectLbl', 'motive_id', 'motiveLbl', 'comments', 'status', 'created_date', 'closed_date', 'priority', 'contact_medium'];

	/**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
	
	protected $guarded = [];
}
