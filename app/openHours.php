<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class openHours extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'open_hours';

	/**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['mondayAmOpen', 'mondayAmClose', 'mondayPmOpen', 'mondayPmClose', 
    					   'tuesdayAmOpen', 'tuesdayAmClose', 'tuesdayPmOpen', 'tuesdayPmClose', 
    					   'wednesdayAmOpen', 'wednesdayAmClose', 'wednesdayPmOpen', 'wednesdayPmClose', 
    					   'thursdayAmOpen', 'thursdayAmClose', 'thursdayPmOpen', 'thursdayPmClose', 
    					   'fridayAmOpen', 'fridayAmClose', 'fridayPmOpen', 'fridayPmClose', 
    					   'saturdayAmOpen', 'saturdayAmClose', 'saturdayPmOpen', 'saturdayPmClose', 
    					   'sundayAmOpen', 'sundayAmClose', 'sundayPmOpen', 'sundayPmClose'];
}
