<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseObject extends Model
{
	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'caseobjects';

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['object','externalcrm_object','activated'];

}
