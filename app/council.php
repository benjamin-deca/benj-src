<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Council extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'councils';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['fullname','address','city','zipcode','country','email','landlinephone','logo','website','mayorName','mayorPhone','mayorMobilePhone','mayorEmail','open_hours_id'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];
	
	protected $guarded = [];

    public function openHours()
    {
        return $this->belongsTo('App\openHours');
    }
}
