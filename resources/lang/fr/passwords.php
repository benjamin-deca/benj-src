<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Les mots de passe doivent faire au minimum 8 caractères.',
    'user' => 'Adresse email inexistante.',
    'token' => 'Adresse email inexistante.',
    'sent' => 'Le lien de réinitialisation de votre mot de passe vient de vous être envoyé par email.',
    'reset' => 'Your password has been reset!',

];
