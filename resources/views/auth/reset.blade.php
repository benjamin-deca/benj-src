<!-- resources/views/auth/reset.blade.php -->
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Decacity</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keyword" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../../plugins/bootstrap/bootstrap.css" rel="stylesheet">
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="../../css/style.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
<body>
<div class="container-fluid">
	<div id="page-login" class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
			<div class="box">
				<div class="box-content">
					 <form method="POST" id="resetPasswordForm" action="./reset">
						{!! csrf_field() !!}
						<input type="hidden" name="token" value="{{ $token }}">
						<div class="text-center">
							<h3 class="page-header">R&eacute;initialiser votre mot de passe</h3>
						</div>
						@if ($errors->has('email'))
							<div class="form-group has-error">
							@else
							<div class="form-group">
						@endif
							<label class="control-label">Nom d'utilisateur (Email)</label>
							<input type="email" class="form-control" name="email" value="{{ old('email') }}"/>
							@if ($errors->has('email'))
								@foreach ($errors->get('email') as $error)
									<small class="help-block col-sm-11">{{ $error }}</small>
								@endforeach
							@endif
						</div>
						@if ($errors->has('password'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
						@endif
							<label class="control-label">Nouveau mot de passe</label>
							<input type="password" class="form-control" name="password" id="password" />
							@if ($errors->has('password'))
								@foreach ($errors->get('password') as $error)
									<small class="help-block col-sm-11">{{ $error }}</small>
								@endforeach
							@endif
						</div>
						@if ($errors->has('password'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
						@endif
							<label class="control-label">Confirmation du mot de passe</label>
							<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" />
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-success">R&eacute;initialiser mon mot de passe</button>
						</div>
					</form>
				</div>
			</div>
			@include('footer-copyright')
		</div>
	</div>
</div>
</body>
</html>