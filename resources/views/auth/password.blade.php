<!-- resources/views/auth/password.blade.php -->
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Decacity</title>
	<meta name="description" content="">
	<meta name="author" content="Decacity">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../plugins/bootstrap/bootstrap.css" rel="stylesheet">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
	<link href="../css/style.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
			<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
			<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container-fluid">
		<div id="page-login" class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
				<div class="text-right">
					<a href="../" class="txt-default">Accueil</a> | 
					<a href="../auth/register" class="txt-default">Cr&eacute;er un compte ?</a>
				</div>
				<div class="box">
					<div class="box-content">
						<form id="emailResetPasswordForm" method="POST" action="./email">
							{!! csrf_field() !!}
							<div class="text-center">
								<h2 class="page-header">Mot de passe</h2>
							</div>
							@if ($errors->has('email'))
								<div class="form-group has-error">
							@else
								<div class="form-group">
							@endif
							<label class="col-xs-12 control-label">Merci de saisir votre email</label>
							<div class="col-xs-12">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
							@if ($errors->has('email') && $errors->first('email') != 'Votre login et/ou votre mot de passe est invalide.')
								@foreach ($errors->get('email') as $error)
									<small class="help-block col-xs-12">{{ $error }}</small>
								@endforeach
							@endif
							</div>
							<br />	<br /> <br />					
							<div class="text-center">
								<button id="submitEmail" type="submit" class="btn btn-success">R&eacute;initialiser mon mot de passe</button>
							</div>
						</form>
					</div>
				</div>
				@include('footer-copyright')
			</div>
		</div>
	</div>
	
	@if (session()->has('status'))
		<div class="flash alert-success">			
			<p>{{ session('status') }}</p>		
		</div>	
	@endif
</body>
</html>