<!-- resources/views/auth/register.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Decacity - Creer un compte citoyen</title>
	<meta name="description" content="">
	<meta name="author" content="Decacity">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../plugins/bootstrap/bootstrap.css" rel="stylesheet">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
	<link href="../css/style.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
			<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
			<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="container-fluid">
	<div id="page-login" class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-6 col-md-offset-3">
			<div class="text-center">
			<!--
				<h3 class="">
					<img src="" title="Decacity - Portail de services citoyen" alt="Portail de services citoyen"/>
					Decacity
					<br />
					<img src="<?php echo config('council.logo'); ?>" />&nbsp;<?php echo config('council.fullname'); ?>
				</h3>
			-->
			</div>
			<div class="text-right">
				<a href="../" class="txt-default">Accueil</a> | 
				<a href="../home" class="txt-default">J'ai d&eacute;j&agrave; un compte</a>
			</div>
			<div class="box">
				<div class="box-content">
					<form id="loginForm" method="post" action="./register" class="form-horizontal">
					 {!! csrf_field() !!}	 
					
						<div class="text-center">
							<h2 class="">Cr&eacute;er un compte</h2>
						</div>
						<fieldset>
							<legend>Identification</legend>
							
							@if ($errors->has('title'))
							<div class="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Civilit&eacute; *</label>
								<div class="col-sm-4">
									<select class="form-control" name="title" value="{{ old('title') }}">
										<option value=" ">Non renseign&eacute;e</option>
										<option value="M">Monsieur</option>
										<option value="MME">Madame</option>
										<option value="MLLE">Mademoiselle</option>
									</select>
								</div>
								@if ($errors->has('title'))
									@foreach ($errors->get('title') as $error)
											<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('lastname'))
							<div class="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Nom *</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}"/>
								</div>
								@if ($errors->has('lastname'))
									@foreach ($errors->get('lastname') as $error)
											<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('firstname'))
							<div class="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Pr&eacute;nom *</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}"/>
								</div>
								@if ($errors->has('firstname'))
									@foreach ($errors->get('firstname') as $error)
											<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
						</fieldset>
						<fieldset>
							<legend>Mon compte</legend>
							
							@if ($errors->has('email'))
							<div class="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Nom d'utilisateur (Email) *</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="email" value="{{ old('email') }}"/>
								</div>
								@if ($errors->has('email'))
									@foreach ($errors->get('email') as $error)
										<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('password'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Mot de passe *</label>
								<div class="col-sm-5">
									<input type="password" class="form-control" name="password" id="password" />
								</div>
								@if ($errors->has('password'))
									@foreach ($errors->get('password') as $error)
										<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('password'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Confirmation du mot de passe *</label>
								<div class="col-sm-5">
									<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" />
								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend>Coordonn&eacute;es</legend>
							
							@if ($errors->has('landlinephone'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">T&eacute;l&eacute;phone fixe</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="landlinephone" value="{{ old('landlinephone') }}" maxlength="10"/>
								</div>
								@if ($errors->has('landlinephone'))
									@foreach ($errors->get('landlinephone') as $error)
										<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('mobilephone'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">T&eacute;l&eacute;phone mobile</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="mobilephone" value="{{ old('mobilephone') }}" maxlength="10"/>
								</div>
								@if ($errors->has('mobilephone'))
									@foreach ($errors->get('mobilephone') as $error)
										<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('address'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Adresse</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="address" value="{{ old('address') }}"/>
								</div>
								@if ($errors->has('address'))
									@foreach ($errors->get('address') as $error)
										<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('zipcode'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Code postal</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}" maxlength="5"/>
								</div>
								@if ($errors->has('zipcode'))
									@foreach ($errors->get('zipcode') as $error)
										<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							@if ($errors->has('city'))
							<div class ="form-group has-error">
							@else
							<div class="form-group">
							@endif
								<label class="col-sm-3 control-label">Ville</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="city" value="{{ old('city') }}"/>
								</div>
								@if ($errors->has('city'))
									@foreach ($errors->get('city') as $error)
										<small class="help-block col-sm-9 col-sm-offset-3">{{ $error }}</small>
									@endforeach
								@endif
							</div>
							
							<div class="form-group">
								<input type="hidden" class="form-control" name="country" value="FRA"/>
							</div>
						</fieldset>
						
						<div class="col-sm-10 col-sm-offset-2">
							<div class="checkbox">
								<label>
									<input type="checkbox"  name="acceptTerms" /> J'accepte les termes et conditions d'utilisation du service
									<i class="fa fa-square-o small"></i>
								</label>
								@if ($errors->has('acceptTerms'))
									@foreach ($errors->get('acceptTerms') as $error)
										<div class ="form-group has-error">
											<small class="help-block col-sm-10">{{ $error }}</small>
										</div>
									@endforeach
								@endif
							</div>
						</div>
						<br /><br /><br />
						<div class="col-sm-8 col-sm-offset-2">
							{!! app('captcha')->display(); !!}
							@if ($errors->has('g-recaptcha-response'))
								@foreach ($errors->get('g-recaptcha-response') as $error)
									<div class ="form-group has-error">
										<small class="help-block col-sm-8">{{ $error }}</small>
									</div>
								@endforeach
							@endif
						</div>
						<br />
						<br /><br />
						<br /><br />
						<div class="text-center">
							<button type="submit" class="btn btn-success">Cr&eacute;er un compte</button>
						</div>
					</form>
				</div>
			</div>
			@include('footer-copyright')
		</div>
	</div>
</div>
</body>
</html>