<!-- resources/views/auth/login.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Decacity</title>
	<meta name="description" content="">
	<meta name="author" content="Decacity">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../plugins/bootstrap/bootstrap.css" rel="stylesheet">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
	<link href="../css/style.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
			<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
			<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="container-fluid">
	<div id="page-login" class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
			<div class="text-right">
				<a href="../" class="txt-default">Accueil</a> | 
				<a href="./register" class="txt-default">Cr&eacute;er un compte ?</a>
			</div>
			<div class="box">
				<div class="box-content">	
					<form id="loginForm" method="post" action="./login">
						{!! csrf_field() !!}
						<div class="text-center">
							<h2 class="page-header">Connexion</h2>
						</div>
						
						@if ($errors->has('email') && $errors->first('email') == 'Votre login et/ou votre mot de passe est invalide.')
							<div class="form-group has-error">
								<small class="help-block col-xs-12">{{ $errors->first('email') }}</small>
							</div>
							<br>
						@endif
						
						@if ($errors->has('email'))
						<div class="form-group has-error">
						@else
						<div class="form-group">
						@endif
							<label class="col-xs-12 col-sm-4 control-label">Nom d'utilisateur (Email)</label>
							<div class="col-xs-12 col-sm-8">
								<input type="text" class="form-control" name="email" value="{{ old('email') }}"/>
							</div>
							@if ($errors->has('email') && $errors->first('email') != 'Votre login et/ou votre mot de passe est invalide.')
								@foreach ($errors->get('email') as $error)
									<small class="help-block col-xs-12 col-sm-8 col-sm-offset-4">{{ $error }}</small>
								@endforeach
							@endif
						</div>
						
						@if ($errors->has('password') || $errors->first('email') == 'Votre login et/ou votre mot de passe est invalide.')
						<div class="form-group has-error">
						@else
						<div class="form-group">
						@endif
							<label class="col-xs-12 col-sm-4 control-label">Mot de passe</label>
							<div class="col-xs-12 col-sm-8">
								<input type="password" class="form-control" name="password" id="password" />
							</div>
							@if ($errors->has('password'))
								@foreach ($errors->get('password') as $error)
									<small class="help-block col-sm-8 col-sm-offset-4">{{ $error }}</small>
								@endforeach
							@endif
							<div class="col-xs-12 col-sm-8 col-sm-offset-4">
								<a href="../password/email" class="txt-default">Mot de passe perdu ?</a>
							</div>
						</div>
						<div class="col-xs-12 col-sm-10 col-sm-offset-1">
							<div class="checkbox">
								<label>
									<input type="checkbox"  name="remember" />Se souvenir de moi ?
									<i class="fa fa-square-o small"></i>
								</label>
							</div>
						</div>						
						 
						<div class="text-center">
							<button type="submit" class="btn btn-success">Se connecter</button>
						</div>
					</form>
				</div>
			</div>
			@include('footer-copyright')
		</div>
	</div>
</div>
@if (isset($message))
	<div class="flash alert-success">			
		<p>{{ $message }}</p>		
	</div>
@endif	
</body>
</html>