@extends('layout')

@section('content')

<?php

use Illuminate\Support\Facades\DB as DB;
use Davispeixoto\Laravel5Salesforce\Salesforce;
use Illuminate\Config\Repository;
use Illuminate\Translation\FileLoader;

try
{
	// La configuration Salesforce
	$configSFDC = session('sfdcConfig');

	//specify the environment to load
	$environment = 'local';

	//create a new config object
	$config = new Illuminate\Config\Repository($configSFDC, $environment);
	
	$mySFDC = new Salesforce($config);
	$customerId = session('userExternalId');
	
	// On récupère les données de l'utilisateur
	$accountResponse = $mySFDC->sfh->retrieve('Id, Salutation, FirstName, LastName, Email, Phone, MobilePhone, Birthdate, Adresse__c, Code_Postal__c, Ville__c, Pays__c, Situation_maritale__c','Contact',array($customerId));

	// Les requêtes SQL pour récupérer les objets et motifs de la demande
	$objets = DB::table('caseobjects')->where('activated','=',1)->get();
	
	/*
	if (isset($_POST['customerId']) && !empty($_POST['customerId'])) 
	{
		$records = array();
		$records[0] = new stdclass();
		$records[0]->DECAMMO__Personne__c = $_POST['customerId']; // ID PRESONNE
		$records[0]->DECAMMO__Moyen_de_contact__c = 'Portail'; // MOYEN DE CONTACT
		$records[0]->DECAMMO__Motif_1__c = $_POST['d_object']; // OBJET
		$records[0]->DECAMMO__Motif_2__c = $_POST['d_motif']; // MOTIF
		$records[0]->DECAMMO__Statut__c = 'A traiter'; // STATUT
		$records[0]->DECAMMO__Priorite__c = 'Moyenne'; // PRIORITE
		$records[0]->DECAMMO__Commentaires__c = $_POST['customerComment']; // COMMENTAIRES
		$response = $mySforceConnection->create($records, 'DECAMMO__Demande__c');
	}
	*/

} catch (Exception $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}
?>
<div id="dialogForm" class="modal fade" role="dialog"></div>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index.html">Accueil</a></li>
			<li><a href="#">Mes demandes</a></li>
			<li><a href="#">Nouvelle demande</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Mes coordonn&eacute;es</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{ $accountResponse[0]->Salutation or ' ' }} {{ $accountResponse[0]->FirstName . " " . $accountResponse[0]->LastName }}</h4>
					<p>
						<div class="form-group">
							<label class="col-sm-3 control-label">T&eacute;l&eacute;phone</label>
								<div class="col-sm-5">
									{{ $accountResponse[0]->Phone or 'non renseign&eacute;' }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-3 control-label">T&eacute;l&eacute;phone mobile</label>
								<div class="col-sm-5">
									{{ $accountResponse[0]->MobilePhone or 'non renseign&eacute;' }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email</label>
								<div class="col-sm-5">
									{{ $accountResponse[0]->Email }}
								</div>
						</div>
						<br>
					</p>
					<button type="button" id ="modify-usercase" class="btn btn-primary" id="modifyCoordCase" onclick='window.location.href="account";'>
						<span>
							<i class="fa fa-pencil-square-o"></i>
							Modifier mes coordonnées
						</span>
					</button>
				</div>
				<br/>
				<div class="card address">
					<h4 class="page-header">Votre adresse</h4>
					<p>
						<span>{{ $accountResponse[0]->Adresse__c or ' ' }}</span> <br>
						<span>{{ $accountResponse[0]->Code_Postal__c or ' '}}</span>
						<span>{{ $accountResponse[0]->Ville__c or ' '}}</span>
						<span>{{ $accountResponse[0]->Pays__c or ' '}}</span>
					</p>
					</div>
			</div>
		</div>
	</div>
</div>
<div class="row"> 
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Nouvelle demande</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="#ajax/deca-addDemande.php" class="form-horizontal">
					<legend>Ma demande</legend>
					<div class="form-group">
						<label class="col-sm-3 control-label">Objet</label>
						<div class="col-sm-5">
							<select class="populate placeholder" name="d_object" id="d_object">
								<option value="">-- S&eacute;lectionner un objet --</option>
								@foreach($objets as $objet)
									<option value="{{ $objet->id }}">{{ $objet->object }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Motif</label>
						<div class="col-sm-5">
							<select class="populate placeholder" name="d_motif" id="d_motif">
								<option value="">-- S&eacute;lectionner un motif --</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-5">
							<button type="submit" class="btn btn-primary" id="validateCase">
								<span>
									<i class="fa fa-pencil-square-o"></i>
									Valider
								</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#d_motif').select2();
	$('#d_object').select2();
}

$(document).ready(function() {
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
	
	DialogCase();
	
	$("#d_object").change(function() 
	{
		var $motifs = $("#d_motif");
		
		$motifs.empty();
		$motifs.append('<option value="" selected="selected">-- S&eacute;lectionner un motif --</option>');
		$motifs.select2();

		$.getJSON("getMotifsFromObject/" + $("#d_object").val(), function(data) 
		{
			$.each(data, function(index, value) 
			{
				$motifs.append('<option value="' + index +'">' + value + '</option>');
			});
		});
	});
	
	
});
</script>
@stop