@extends('layout')

@section('content')
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

<?php

use Davispeixoto\Laravel5Salesforce\Salesforce;
use Illuminate\Config\Repository;
use Illuminate\Translation\FileLoader;

try {
	// La configuration Salesforce
	$configSFDC = session('sfdcConfig');

	//specify the environment to load
	$environment = 'local';

	//create a new config object
	$config = new Illuminate\Config\Repository($configSFDC, $environment);
	
	$mySFDC = new Salesforce($config);
	$customerId = session('userExternalId');
	
	//Recuperation des demandes de la personne
	$casesQuery = "SELECT Name, RecordTypeId, CreatedDate, Moyen_de_contact__c, Motif_1__c, Motif_2__c, Statut__c, Date_de_cloture__c from Demande__c where Personne__r.Id = '" . $customerId . "'";
	$casesResponse = $mySFDC->sfh->query($casesQuery);
	
} catch (Exception $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}
?>
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="./home">Accueil</a></li>
			<li><a href="./cases">Mes demandes</a></li>
			<li><a href="./cases">Toutes mes demandes</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Mes demandes</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<button type="button" onclick='window.location.href="addCase";' class="btn btn-primary" id="newCase">
					<span>
						<i class="fa fa-pencil-square-o"></i>
						Nouvelle demande
					</span>
				</button>
				<table id="tableCase" class="table table-striped table-hover table-heading table-datatable dataTable">
					<thead>
						<tr>
							<th>N&ordm;</th>
							<th>Date de création</th>
							<th>Moyen de contact</th>
							<th>Objet</th>
							<th>Motif</th>
							<th>Statut</th>
							<th>Date de cl&ocirc;ture</th>
						</tr>
					</thead>
					<tbody>
					@foreach($casesResponse->records as $record)
						@if($record->Statut__c == 'Clos')
						<tr style="background-color:limegreen">
						@else
						<tr>
						@endif
							<td><a href='./editCase.blade.php?caseId={{ $record->RecordTypeId }}'>{{ $record->Name }}</a></td>
							<td><?php $dateArray = explode("T", $record->CreatedDate); 
								$date = date_format(date_create($dateArray[0]), 'd/m/Y');?>
								{{ $date or ' ' }}</td>
							<td>{{ $record->Moyen_de_contact__c or ' ' }}</td>
							<td>{{ $record->Motif_1__c or ' ' }}</td>
							<td>{{ $record->Motif_2__c or ' ' }}</td>
							<td>{{ $record->Statut__c or ' ' }}</td>
							<td><?php $dateClos= '';
									if (isset($record->Date_de_cloture__c)) {
									$dateClosArray = explode("T", $record->Date_de_cloture__c);
									$dateClos = date_format(date_create($dateClosArray[0]), 'd/m/Y');
									}?>
								{{ $dateClos or ' ' }}</td>
						</tr>
					@endforeach 
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	LoadDatatableCase();
});
</script>
@stop