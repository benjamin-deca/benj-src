<!-- Boite de dialogue "Nouvelle demande" -->

<div id="dialogCase" class="modal-dialog">
    <!-- Modal content-->
	<div class="modal-content">     
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Nouvelle demande</h4>
		</div>
		<div class="modal-body">
			<form>
				<input type='hidden' id="userExternalId" value="{{session('userExternalId')}}" />
				<input type='hidden' id="caseGravity" value="" />
				<input type='hidden' id="caseCreationDate" value="" />
				{!! csrf_field() !!}
				<fieldset>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Objet</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="caseObject" placeholder="">
						</div>
					</div>
					<br>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Motif</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="caseMotive" placeholder="">
						</div>
					</div>
					<br>
					@if (isset($_POST['landlinephone']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="name" class="col-sm-4 control-label">T&eacute;l&eacute;phone fixe</label>
						<div class="col-sm-7">
							<input type="text" name="phone" id="phone_form" value="" class="form-control" maxlength="10">
						</div>
						@if (isset($_POST['landlinephone']))
							@foreach ($_POST['landlinephone'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					@if (isset($_POST['caseComments']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="name" class="col-sm-4 control-label">Commentaires</label>
						<div class="col-sm-7">
							<textarea type="text" name="caseComments" id="caseComments" value="" class="form-control">
						</div>
						@if (isset($_POST['caseComments']))
							@foreach ($_POST['caseComments'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					<!-- TODO Gerer les PJ --> 
					
					
				</fieldset>
			</form>
		</div>
		<div class="modal-footer">
			<button id="submitCoord" type="button" class="btn btn-primary">Modifier</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>

<!-- Boite de dialogue "Confirmation demande -->

<div id="dialogCaseOk" class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Nouvelle demande</h4>
		</div>
		<div class="modal-body">
			<p>
				Votre demande a bien été soumise
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" onclick='window.location.href="case";'>Ok</button>
		</div>
	</div>
</div>