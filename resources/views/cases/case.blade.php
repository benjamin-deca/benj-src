@extends('layout')

@section('content')
<?php
	session_start();
	define("USERNAME", "maxime-dev@decacity.com");
	define("PASSWORD", "g8h6a9z7");
	define("SECURITY_TOKEN", "iPbMRvgGtFZpSTht6yoUHTyHX");
	require_once ("../soapclient/SforceEnterpriseClient.php");
	try
	{
        $mySforceConnection = new SforceEnterpriseClient();
		$mySforceConnection->createConnection("/home/mougne/www/sites/deca/soapclient/decacity.wsdl.xml");
		if (isset($_SESSION['enterpriseSessionId'])) 
		{
	        $location = $_SESSION['enterpriseLocation'];
			$sessionId = $_SESSION['enterpriseSessionId'];
			$mySforceConnection->setEndpoint($location);
			$mySforceConnection->setSessionHeader($sessionId);
	    } 
		else 
		{
			$mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);
			$_SESSION['enterpriseLocation'] = $mySforceConnection->getLocation();
			$_SESSION['enterpriseSessionId'] = $mySforceConnection->getSessionId();
	    }
		
		if (isset($_POST['customerId']) && !empty($_POST['customerId'])) 
		{
			$records = array();
			$records[0] = new stdclass();
			$records[0]->DECAMMO__Personne__c = $_POST['customerId']; // ID PRESONNE
			$records[0]->DECAMMO__Moyen_de_contact__c = 'Portail'; // MOYEN DE CONTACT
			$records[0]->DECAMMO__Motif_1__c = $_POST['d_object']; // OBJET
			$records[0]->DECAMMO__Motif_2__c = $_POST['d_motif']; // MOTIF
			$records[0]->DECAMMO__Statut__c = 'A traiter'; // STATUT
			$records[0]->DECAMMO__Priorite__c = 'Moyenne'; // PRIORITE
			$records[0]->DECAMMO__Commentaires__c = $_POST['customerComment']; // COMMENTAIRES
			$response = $mySforceConnection->create($records, 'DECAMMO__Demande__c');
		}

	} catch (Exception $e) {
		echo "Exception ".$e->faultstring."<br/><br/>\n";
		echo "Last Request:<br/><br/>\n";
		echo $mySforceConnection->getLastRequestHeaders();
		echo "<br/><br/>\n";
	    echo $mySforceConnection->getLastRequest();
	    echo "<br/><br/>\n";
	    echo "Last Response:<br/><br/>\n";
	    echo $mySforceConnection->getLastResponseHeaders();
	    echo "<br/><br/>\n";
	    echo $mySforceConnection->getLastResponse();
	}
?>
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index.html">Accueil</a></li>
			<li><a href="#">Demandes</a></li>
			<li><a href="#">Nouvelle demande</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-search"></i>
					<span>Nouvelle demande</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
			<?php
				if (isset($_POST['customerId']) && !empty($_POST['customerId'])) 
				{
					echo ($response[0]->success == 1)
	                    ? "Votre demande a bien &eacute;t&eacute; envoy&eacute;e"
	                    : "Error: ".$response[0]->errors->message."<br/>\n";
				}
				else
				{	
			?>
				<form id="defaultForm" method="post" action="#ajax/deca-addDemande.php" class="form-horizontal">
					
					<fieldset>
						<legend>Ma demande</legend>
						<div class="form-group">
							<label class="col-sm-3 control-label">Objet</label>
							<div class="col-sm-5">
								<select class="populate placeholder" name="d_object" id="d_object">
									<option value="">-- S&eacute;lectionner un objet --</option>
									<option value="elections">Elections</option>
									<option value="">Environnement</option>
									<option value="fr">Etat civil</option>
									<option value="ru">Loisirs</option>
									<option value="it">Scolaire</option>
									<option value="Social">Social</option>
									<option value="gb">Services techniques</option>
								</select>
							</div>
						</div><div class="form-group">
							<label class="col-sm-3 control-label">Motif</label>
							<div class="col-sm-5">
								<select class="populate placeholder" name="d_motif" id="d_motif">
									<option value="">-- S&eacute;lectionner un motif --</option>
									<option value="fr">France</option>
									<option value="de">Germany</option>
									<option value="it">Italy</option>
									<option value="jp">Japan</option>
									<option value="ru">Russia</option>
									<option value="gb">United Kingdom</option>
									<option value="us">United State</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="form-styles">Textarea</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows="5" id="wysiwig_simple" name='customerComment'></textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<div class="checkbox">
								<label>
									<input type="checkbox"  name="acceptTerms" /> Accepter les termes et conditions
									<i class="fa fa-square-o small"></i>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" class="btn btn-primary">Envoyer</button>
						</div>
					</div>
				</form>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#d_motif').select2();
	$('#d_object').select2();
}
// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>
@stop