<!-- resources/views/emails/newpassword.blade.php -->

<?php
require_once('../config/council.php');
?>
<p>
	<img src='<?php echo $message->embed(config('council.logo')); ?>' alt='<?php echo config('council.name'); ?>'>
</p>
<p>
	Cher monsieur / Chère madame,
</p>
<p>
	Vous avez demandé la réinitialisation de votre mot de passe de compte citoyen.
</p>
<p>
	Si vous n'êtes pas à l'origine de cette demande, merci de contacter immédiatement votre mairie aux coordonnées suivantes ou de passer par le formulaire de contact de notre site internet :<br>	
{{ config('council.fullname') }}<br>
{{ config('council.landlinephone') }}
</p>
<p>
	Cordialement,
</p>
<p>
{{ config('council.fullname') }}<br>
{{ config('council.landlinephone') }}<br>
{{ config('council.address') }}<br>
{{ config('council.zipcode') }} {{ config('council.city') }}
</p>