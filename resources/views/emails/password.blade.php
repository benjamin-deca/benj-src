<!-- resources/views/emails/password.blade.php -->

<?php
require_once('../config/council.php');
?>
<p>
	<img src='<?php echo $message->embed(config('council.logo')); ?>' alt='<?php echo config('council.name'); ?>'>
</p>
<p>
	Cher monsieur / Chère madame,
</p>
<p>
	Vous avez demandé la réinitialisation de votre mot de passe de compte citoyen.
</p>
<p>
	Pour choisir un nouveau mot de passe, merci de cliquer sur le lien ci-contre : {{ url('password/reset/'.$token) }}
</p>
<p>
	Cordialement,
</p>
<p>
{{ config('council.fullname') }}<br>
{{ config('council.landlinephone') }}<br>
{{ config('council.address') }}<br>
{{ config('council.zipcode') }} {{ config('council.city') }}
</p>