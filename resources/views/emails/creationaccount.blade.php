<!-- resources/views/emails/creationaccount.blade.php -->

<?php
require_once('../config/council.php');
?>
<p>
	<img src='<?php echo $message->embed(config('council.logo')); ?>' alt='<?php echo config('council.name'); ?>'>
</p>
<p>
	Cher monsieur / Chère madame,
</p>
<p>
	Nous vous remercions de la création de votre compte citoyen sur le portail de notre commune.
</p>
<p> Rappel de vos coordonnées :
	<ul> 
	<li>Email : {{ $user->Email }}</li>
	<?php if(isset($user->MobilePhone)) { ?><li>Téléphone mobile : {{ $user->MobilePhone }}</li><?php }?>
	<?php if(isset($user->Phone)) { ?><li>Téléphone : {{ $user->Phone }}</li><?php }?>
	<?php if(isset($user->Adresse__c)) { ?><li>Adresse : {{ $user->Adresse__c }}</li><?php }?>
	<?php if(isset($user->Code_Postal__c)) { ?><li>Code postal : {{ $user->Code_Postal__c }}</li><?php }?>
	<?php if(isset($user->Ville__c)) { ?><li>Ville : {{ $user->Ville__c }}</li><?php }?>
	</ul>
</p>
<p>
	Sachez que vous pouvez suivre à tout moment l'avancement de vos demandes sur notre portail citoyen en cliquant sur le lien ci-contre : <a href="{{ url('') }}">{{ url('') }}</a>
</p>
<p>
	Cordialement,
</p>
<p>
{{ config('council.fullname') }}<br>
{{ config('council.landlinephone') }}<br>
{{ config('council.address') }}<br>
{{ config('council.zipcode') }} {{ config('council.city') }}
</p>