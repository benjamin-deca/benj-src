<!DOCTYPE html>
<html lang="fr">
	<head>
		<base href="<?php echo session('portalConfig.BASE_HREF'); ?>" />
		<meta charset="utf-8">
		<title>Decacity</title>
		<meta name="description" content="description">
		<meta name="author" content="Decacity">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<link href="plugins/bootstrap/bootstrap.css" rel="stylesheet">
		<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
		<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
		<link href="plugins/xcharts/xcharts.min.css" rel="stylesheet">
		<link href="plugins/select2/select2.css" rel="stylesheet">
		<link href="plugins/justified-gallery/justifiedGallery.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<link href="plugins/chartist/chartist.min.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="plugins/jquery/jquery.min.js"></script>
		<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="plugins/bootstrap/bootstrap.min.js"></script>
		<script src="plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
		<script src="plugins/tinymce/tinymce.min.js"></script>
		<script src="plugins/tinymce/jquery.tinymce.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
		<script src="http://fgnass.github.io/spin.js/spin.min.js"></script>
		<!-- All functions for this theme + document.ready processing -->
		<script src="js/devoops.js"></script>
		<script src="js/portal.js"></script>
		<script type="text/javascript">
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		</script>
	</head>
<body id="targetSpinner">
<!--Start Header-->
<div id="modalbox">
	<div class="devoops-modal">
		<div class="devoops-modal-header">
			<div class="modal-header-name">
				<span>Basic table</span>
			</div>
			<div class="box-icons">
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="devoops-modal-inner">
		</div>
		<div class="devoops-modal-bottom">
		</div>
	</div>
</div>
<header class="navbar">
	<div class="container-fluid expanded-panel">
		<div class="row">
			<div id="logo" class="col-xs-2">
				<a href="#" class="about"><img src='img/decacity-logo-mini.png' alt='Decacity - Portail de services citoyens'/></a>
			</div>
			<div id="top-panel" class="col-xs-10">
				<div class="row">
					<div class="col-xs-2"> &nbsp;
					</div>
					<div class="col-xs-4 col-sm-4 top-panel-right">
						<a href="home" class=""><img src="img/mairie/{{ session('council')->logo }}" alt="{{ session('council')->fullname }}">&nbsp; {{ session('council')->fullname }}</a>
					</div>
					<div class="col-xs-4 col-sm-4 top-panel-right">					
						<ul class="nav navbar-nav pull-right panel-menu">
							<?php 
							if(session('portalConfig.MOD_BILL_ACTIVATED') == "1")
							{
							?>
							<li class="hidden-xs">
								<a href="" class="modal-link">
									<i class="fa fa-bell"></i>
									<span class="badge">0</span>
								</a>
							</li>
							<?php
							}
							if(session('portalConfig.MOD_EVENT_ACTIVATED') == "1")
							{
							?>
							<li class="hidden-xs">
								<a class="ajax-link" href="">
									<i class="fa fa-calendar"></i>
									<span class="badge">0</span>
								</a>
							</li>
							<?php
							}
							?>
							<!--
							<li class="hidden-xs">
								<a href="ajax/page_messages.html" class="ajax-link">
									<i class="fa fa-envelope"></i>
									<span class="badge">0</span>
								</a>
							</li>
							-->
							<li class="dropdown">
								<a href="#" class="dropdown-toggle account" data-toggle="dropdown">
									<?php
										if(session('portalConfig.MOD_AVATAR_ACTIVATED') == "1")
										{
									?>
									<div class="avatar">
										<img src="img/avatar/{{ session('user')->avatar or ''}}" class="img-circle" alt="avatar" />
									</div>
									<?php
										}
									?>
									<i class="fa fa-angle-down pull-right"></i>
									<div class="user-mini pull-right">
										<span class="welcome">Bienvenue,</span>
										<span>{{ session('user')->firstname }}</span>
									</div>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="users/{{ session('user')->email }}/edit">
											<i class="fa fa-user"></i>
											<span>Profil</span>
										</a>
									</li>
									<li>
										<a href="" class="ajax-link">
											<i class="fa fa-envelope"></i>
											<span>Messages</span>
										</a>
									</li>
									<li>
										<a href="auth/logout">
											<i class="fa fa-power-off"></i>
											<span>D&eacute;connexion</span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!--End Header-->
<!--Start Container-->
<div id="main" class="container-fluid">
	<div class="row">
		<div id="sidebar-left" class="col-xs-2 col-sm-2">
			<ul class="nav main-menu">
				@if (session('portalConfig.MOD_DASHBOARD_ACTIVATED') == "1")
					<li>
						<a href="home" class="active">
							<i class="fa fa-dashboard"></i>
							<span class="hidden-xs">Accueil</span>
						</a>
					</li>
				@endif
				<li>
					<a href="account" class="">
						<i class="fa fa-user"></i>
						 <span class="hidden-xs">Mon compte</span>
					</a>
				</li>
				<li>
					<a href="council" class="">
						<i class="fa fa-home"></i>
						<span class="hidden-xs">Ma mairie</span>
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-pencil-square-o"></i>
						<span class="hidden-xs">Mes demandes</span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="" href="demands">
								<i class="fa fa-bars"></i>
								<span class="hidden-xs">Toutes mes demandes</span>
							</a>
						</li>
						<li>
							<a class="" href="demands/create">
								<i class="fa fa-pencil-square-o"></i>
								<span class="hidden-xs">Nouvelle demande</span>
							</a>
						</li>
					</ul>
				</li>
				@if(session('portalConfig.MOD_EVENT_ACTIVATED') == "1")
				<li>
					<a class="ajax-link" href="ajax/calendar.html">
						 <i class="fa fa-calendar"></i>
						 <span class="hidden-xs">Mes &eacute;v&egrave;nements</span>
					</a>
				</li>
				@endif
				@if (session('portalConfig.MOD_BILL_ACTIVATED') == "1")
				<li>
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-eur"></i>
						 <span class="hidden-xs">Mes r&egrave;glements</span>
					</a>
				</li>
				@endif
				@if ((session('user')->is_admin_deca == 1) || (session('user')->is_admin == 1))
				<li class="dropdown">
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-cog"></i>
						 <span class="hidden-xs">Administration</span>
					</a>
					<ul class="dropdown-menu">
						<li><a class="" href="councils">Mairie</a></li>
						<li><a class="" href="portalConfigs">Configuration</a></li>
						<li><a class="" href="users">Utilisateurs</a></li>
					</ul>
				</li>
				@endif
				<li>
					<a href="auth/logout">
						<i class="fa fa-power-off"></i>
						<span class="hidden-xs">D&eacute;connexion</span>
					</a>
				</li>
			</ul>
		</div>
		<!--Start Content-->
		<div id="content" class="col-xs-10 col-sm-10">
			<div id="about">
				<div class="about-inner">
					<h4 class="page-header">Decacity - Votre portail de services citoyen</h4>
					<p>Site web - <a href="http://www.decacity.com" target="_blank">http://www.decacity.com</a></p>
					<p>Email - <a href="mailto:contact@decacity.com">contact@decacity.com</a></p>
				</div>
			</div>
			<div class="preloader">
				<img src="img/devoops_getdata.gif" class="devoops-getdata" alt="preloader"/>
			</div>
			<!-- <div id="ajax-content"></div> -->
			@yield('content')
		</div>
		<!--End Content-->
	</div>
</div>
<!--End Container-->
</body>
</html>
