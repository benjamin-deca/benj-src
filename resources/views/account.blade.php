@extends('layout')

@section('content')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmKGzyoTvJMdOJ6hC25saSHF2RF-cSyyI"></script>
<script src="http://fgnass.github.io/spin.js/spin.min.js"></script>
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>



<?php

use Davispeixoto\Laravel5Salesforce\Salesforce;
use Illuminate\Config\Repository;
use Illuminate\Translation\FileLoader;

try {

	// La configuration Salesforce
	$configSFDC = session('sfdcConfig');
	
	//specify the environment to load
	$environment = 'local';

	//create a new config object
	$config = new Illuminate\Config\Repository($configSFDC, $environment);
	$mySFDC = new Salesforce($config);
	
	// On récupère l'Id courant de l'utilisateur
	$customerId = session('userExternalId');
	
	// On récupère les données de l'utilisateur
	$accountResponse = $mySFDC->sfh->retrieve('Id, Salutation, FirstName, LastName, Email, Phone, MobilePhone, Birthdate, Adresse__c, Code_Postal__c, Ville__c, Pays__c, Situation_maritale__c','Contact',array($customerId));

	// On récupère les données liées au foyer
	$foyerQuery = "SELECT Personne_liee__r.Id, Personne_liee__r.Name, Personne_liee__r.Birthdate, Type_de_lien__c from Lien__c where Personne__r.Id = '" . $customerId . "' and Foyer__c = TRUE ORDER BY Personne_liee__r.Birthdate ASC";
	$foyerResponse = $mySFDC->sfh->query($foyerQuery);

	// On récupère les données liées aux associations
	$assoQuery = "SELECT Association__r.Id, Depuis_le__c, Fonction__c, Association__r.Name from Lien_Association_Personne__c where Personne__r.Id = '" . $customerId . "' ORDER BY Association__r.Name ASC";
	$assoResponse = $mySFDC->sfh->query($assoQuery);
	
		
} catch (Exception $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}
?>

<div id="dialogForm" class="modal fade" role="dialog"></div>

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="./home">Accueil</a></li>
			<li><a href="./account">Mon compte</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-7">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Mon compte</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{ $accountResponse[0]->Salutation or ' ' }} {{ $accountResponse[0]->FirstName . " " . $accountResponse[0]->LastName }}</h4>
					<p>
						<div class="form-group">
							<label class="col-md-4 control-label">Situation maritale</label>
								<div class="col-md-8">
									{{ $accountResponse[0]->Situation_maritale__c or 'non renseign&eacute;e' }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-md-4 control-label">Date de naissance</label>
								<div class="col-md-8">
									<?php
										$date = 'non renseign&eacute;e';
										if(isset($accountResponse[0]->Birthdate)) {
											$dateString = strval($accountResponse[0]->Birthdate);
											$date = date_format(date_create($dateString), 'd/m/Y');
										}
									?>
									{{ $date or 'non renseign&eacute;e' }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-4 control-label">T&eacute;l&eacute;phone</label>
								<div class="col-sm-8">
									{{ $accountResponse[0]->Phone or 'non renseign&eacute;' }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-4 control-label">Portable</label>
								<div class="col-sm-8">
									{{ $accountResponse[0]->MobilePhone or 'non renseign&eacute;' }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-4 control-label">Email</label>
								<div class="col-sm-8">
									{{ $accountResponse[0]->Email }}
								</div>
						</div>
						<br>
					</p>
					<button type="button" id="modify-user" class="btn btn-primary">
						<span>
							<i class="fa fa-pencil-square-o"></i>							
							Modifier mes coordonnées
						</span>
					</button>
				</div>
				<br/>
				<div class="card address">
					<h4 class="page-header">Votre adresse</h4>
					<p>
						<span>{{ $accountResponse[0]->Adresse__c or ' ' }}</span> <br>
						<span>{{ $accountResponse[0]->Code_Postal__c or ' '}}</span>
						<span>{{ $accountResponse[0]->Ville__c or ' '}}</span>
						<span>{{ $accountResponse[0]->Pays__c or ' '}}</span>
					</p>
					<button type="button" id="modify-address" class="btn btn-primary">
						<span>
							<i class="fa fa-pencil-square-o"></i>
							Modifier mon adresse
						</span>
					</button>
					<div class="map" id="map-canvas" style="height: 450px;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-5">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-users"></i>
					<span>Foyer</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<table id="tableFoyer" class="table table-hover">
					<thead>
					<tr>
						<th>Nom</th>
						<th>Date de naissance</th>
						<th>Type</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
						@foreach($foyerResponse->records as $record)
						<tr>
							<th>{{ $record->Personne_liee__r->Name or ' ' }}</th>
							<th>
								<?php
										$dateNaissanceFoyer = 'non renseign&eacute;e';
										if(isset($record->Personne_liee__r->Birthdate)) {
											$dateString = strval($record->Personne_liee__r->Birthdate);
											$dateNaissanceFoyer = date_format(date_create($dateString), 'd/m/Y');
										}
								?>
								{{ $dateNaissanceFoyer or 'non renseign&eacute;e' }}</th>
							<th>{{ $record->Type_de_lien__c or ' ' }}</th>
							<th><button type="button" value="{{  $record->Personne_liee__r->Id }}" class="deleteLinkFoyer"><i class ="fa fa-trash"></button></th>
						</tr>
						@endforeach
					</tbody>
				</table>	
				<button type="button" id="add-conjoint" class="btn btn-primary">
					<span>
						<i class="fa fa-user-plus"></i>
						Nouveau conjoint
					</span>
				</button>
				<button type="button" id="add-child" class="btn btn-primary">
					<span>
						<i class="fa fa-user-plus"></i>
						Nouvel enfant
					</span>
				</button>
			</div>
		</div>
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-trophy"></i>
					<span>Associations</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<table class="table table-hover" id="tableAsso">
					<thead>
						<tr>
							<th>Assocation</th>
							<th>Fonction</th>
							<th>Depuis le</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($assoResponse->records as $record)
						<tr>
							<th>{{ $record->Association__r->Name or ' ' }}</th>
							<th>{{ $record->Fonction__c or ' ' }}</th>
							<th>
								<?php
										$dateDepuisLeAsso = 'non renseign&eacute;e';
										if(isset($record->Depuis_le__c)) {
											$dateString = strval($record->Depuis_le__c);
											$dateDepuisLeAsso = date_format(date_create($dateString), 'd/m/Y');
										}
								?>
								{{ $dateDepuisLeAsso or 'non renseign&eacute;e' }}
							</th>
							<th><button type="button" value="{{ $record->Association__r->Id }}" class="deleteLinkAssociation"><i class ="fa fa-trash"></button></th>
						</tr>
						@endforeach
					</tbody>
				</table>
				<button type="button" id="add-asso" class="btn btn-primary">
					<span>
						<i class="fa fa-plus-square"></i>
						Nouvelle association
					</span>
				</button>
			</div>
		</div>
	</div>
</div>
<script>
function initialize() {
var geocoder = new google.maps.Geocoder();
var mapOptions = {
    scaleControl: true,
	zoom: 15,
    center: {lat: -34.397, lng: 150.644}
};
var  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

var address = '{{ $accountResponse[0]->Adresse__c or ' ' }},' + ' ' + '{{ $accountResponse[0]->Ville__c or ' ' }},' + ' ' + '{{ $accountResponse[0]->Code_Postal__c or ' ' }}';
geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      $('#map-canvas').html("La carte n'a pas pu s'afficher, veuillez v&eacute;rifier l'adresse.");
    }
 });
}
google.maps.event.addDomListener(window, 'load', initialize);

// Les options du Spinner
var opts = {
  lines: 13 // The number of lines to draw
, length: 28 // The length of each line
, width: 14 // The line thickness
, radius: 42 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
}
var target = document.getElementById('targetSpinner');

$(document).ready(function() {
	LoadDatatableAccount();
	DialogAccount();
	DeleteAssoFoyer();
});
</script>
@stop