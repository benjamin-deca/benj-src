@extends('layout')

@section('content')

<style type="text/css">
  html, body, #map-canvas { height: 100%; margin: 0; padding: 0;}
</style>
<script type="text/javascript"
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmKGzyoTvJMdOJ6hC25saSHF2RF-cSyyI">
</script>

<?php
require_once('../config/council.php');
$address = config('council.address');
$zipcode = config('council.zipcode');
$city = config('council.city');
?>

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="./home">Accueil</a></li>
			<li><a href="#">Ma mairie</a></li>
			<li><a href="./council"><?php echo config('council.name'); ?></a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-home"></i>
					<span>Ma mairie</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{ config('council.fullname') }}</h4>
					<p>
						<div class="form-group">
							<label class="col-sm-3 control-label">T&eacute;l&eacute;phone</label>
								<div class="col-sm-5">
									{{ config('council.landlinephone') }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email</label>
								<div class="col-sm-5">
									<a href="mailto:{{ config('council.email') }}">{{ config('council.email') }}</a>
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-3 control-label">Site internet</label>
								<div class="col-sm-5">
									<a href="{{ config('council.website') }}">{{ config('council.website') }}</a>
								</div>
						</div>
						<br><br>
						<button type="button" onclick='window.location.href="addCase";' class="btn btn-primary" id="contactUs">
							<span>
								<i class="fa fa-pencil-square-o"></i>
								Nous contacter
							</span>
						</button>
					</p>
				</div>
				<div class="card address">
					<h4 class="page-header">Adresse de la mairie</h4>
					<p>
						<span>{{ $address }}</span> <br>
						<span>{{ $zipcode }}</span>
						<span>{{ $city }}</span>
						<span>{{ config('council.country') }}</span>
					</p>
					<div class="map" id="map-canvas" style="height: 450px;">
					</div>
				</div>
				<br/>

			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-clock-o"></i>
					<span>Horaires d'ouvertures</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="open">
					<p>
						<table class="table table-bordered">
							<thead>
								<th>Jours</th>
								<th>Horaires</th>
							</thead>
							<tbody>
								<tr>
									<td>Lundi</td>
									<td>{{ config('council.openMonday') }}</td>
								</tr>
								<tr>
									<td>Mardi</td>
									<td>{{ config('council.openTuesday') }}</td>
								</tr>
								<tr>
									<td>Mercredi</td>
									<td>{{ config('council.openWednesday') }}</td>
								</tr>
								<tr>
									<td>Jeudi</td>
									<td>{{ config('council.openThursday') }}</td>
								</tr>
								<tr>
									<td>Vendredi</td>
									<td>{{ config('council.openFriday') }}</td>
								</tr>
								<tr>
									<td>Samedi</td>
									<td>{{ config('council.openSaturday') }}</td>
								</tr>
								<tr>
									<td>Dimanche</td>
									<td>{{ config('council.openSunday') }}</td>
								</tr>
							</tbody>
						</table>
					</p>
				</div>
			</div>
		</div>
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Maire</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{config('council.mayorName') }}</h4>
					<p>
						<div class="form-group">
							<label class="col-sm-4 control-label">T&eacute;l&eacute;phone</label>
								<div class="col-sm-5">
									{{ config('council.mayorPhone') }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-4 control-label">Mobile</label>
								<div class="col-sm-5">
									{{ config('council.mayorMobilePhone') }}
								</div>
						</div>
						<br>
						<div class="form-group">
							<label class="col-sm-4 control-label">Email</label>
								<div class="col-sm-5">
									<a href="mailto:{{ config('council.mayorEmail') }}">{{ config('council.mayorEmail') }}</a>
								</div>
						</div>
					</p>
					<br>
				</div>
			</div>
		</div>
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-users"></i>
					<span>Conseillers</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">TO DO</h4>
					<p>
						<span>TO DO</span> <br>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function initialize() {
var geocoder = new google.maps.Geocoder();
var mapOptions = {
    scaleControl: true,
	zoom: 15,
    center: {lat: -34.397, lng: 150.644}
  };
  
var  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

var address = '{{ $address }},' + ' ' + '{{ $city }},' + ' ' + '{{ $zipcode }}';
geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      $('#map-canvas').html("La carte n'a pas pu s'afficher, veuillez v&eacute;rifier l'adresse.");
    }
 });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
@stop