<!-- Boite de dialogue "Ajouter un foyer" -->

<div id="dialogFoyer" class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title" id="titleFoyer">Ajouter un membre de votre foyer</h4>
		</div>
		<div class="modal-body">
			<form>
			{!! csrf_field() !!}
				<fieldset>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Civilité</label>
						<div class="col-sm-7">
							<select class="form-control" name="civilite" id="civilite_form" >
								<option value="">-- S&eacute;lectionner une civilité --</option>
								<option value=" ">Non renseign&eacute;e</option>
								<option value="M.">Monsieur</option>
								<option value="Mme.">Madame</option>
								<option value="Mlle.">Mademoiselle</option>
							</select>
						</div>
					</div>
					@if (isset($_POST['lastname']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="name" class="col-sm-4 control-label">Nom *</label>
						<div class="col-sm-7">
							<input type="text" name="lastname" id="lastname_form" value="" class="form-control">
						</div>
						@if (isset($_POST['lastname']))
							@foreach ($_POST['lastname'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					@if (isset($_POST['firstname']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="name" class="col-sm-4 control-label">Prénom *</label>
						<div class="col-sm-7">
							<input type="text" name="firstname" id="firstname_form" value="" class="form-control">
						</div>
						@if (isset($_POST['firstname']))
							@foreach ($_POST['firstname'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					@if (isset($_POST['typelink']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="name" class="col-sm-4 control-label">Type de lien *</label>
						<div class="col-sm-7">
							<select class="form-control" name="type_link" id="type_link_form" >
								<option id="optConjoint">Conjoint</option>
								<option id="optChild">Enfant</option>
							</select>
						</div>
						@if (isset($_POST['typelink']))
							@foreach ($_POST['typelink'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					@if (isset($_POST['birthdate']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="birthdatePicker" class="col-sm-4 control-label">Date de naissance *</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" name="birthdate" id="birthdate_form" placeholder="">
						</div>
						@if (isset($_POST['birthdate']))
							@foreach ($_POST['birthdate'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<div class="form-group">
						<label for="country" class="col-sm-4 control-label">Lieu de naissance</label>
						<div class="col-sm-7">
							<input type="text" name="birthdatePlace" id="birthdatePlace_form" value="" class="form-control">
						</div>
					</div>		
				</fieldset>
			</form>
		</div>
		<div class="modal-footer">
			<button id="submitFoyer" type="button" class="btn btn-primary">Enregistrer</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>

<!-- Boite de dialogue "Foyer ajouté" -->

<div id="dialogFoyerOk" class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Votre foyer</h4>
		</div>
		<div class="modal-body">
			<p>
				Ce membre de votre famille a bien &eacute;t&eacute; ajout&eacute;.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" onclick='window.location.href="account";'>Ok</button>
		</div>
	</div>
</div>