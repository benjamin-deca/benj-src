<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Demande</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{$demand->object or 'Erreur'}}</h4>
					<input type='hidden' name='council_id' id='council_id' value="{{$demand->council_id or ''}}" />
					@if (session('user')->is_admin_deca == 1)
						<div class="row form-group">
							<label class="col-sm-4 col-md-3 control-label">Customer Id</label>
							<div class="col-sm-8 col-md-5">
									<input type="text" readonly class="form-control"  name="contact_id" id="contact_id" value="{{$Demand->contact_id or session('user')->externalcrm_id}}" />
							</div>
						</div>
					@else
						<input type='hidden' name='contact_id' id='contact_id' value="{{$Demand->contact_id or session('user')->externalcrm_id}}" />
					@endif
					@if (isset($Demand->external_id))
						<div class="row form-group">
							<label class="col-sm-4 col-md-3 control-label">N&deg; demande</label>
							<div class="col-sm-8 col-md-5">
								@if (session('user')->is_admin_deca == 1)
									<input type="text" readonly class="form-control"  name="external_id" id="external_id" value="{{$Demand->external_id or ''}}" />
								@else
									{{$Demand->external_id or ''}}
								@endif
							</div>
						</div>
					@endif
					
					@if (session('user')->is_admin_deca == 1)
						@if ($errors->has('status'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
						<label class="col-sm-4 col-md-3 control-label">Statut *</label>
						<div class="col-sm-8 col-md-5">
							<select class="form-control" name="status" id="status">
								<option value="">-- S&eacute;lectionner un statut --</option>
									<?php $selectedStatus = '';
										if (isset($Demand->status)) {
											$selectedStatus = $Demand->status;
										}
										else {
											$selectedStatus = "A traiter";
										}
									?>
									<option value="A traiter" {{ ($selectedStatus == 'A traiter') ? 'selected' : '' }}>A traiter</option>
									<option value="En cours" {{ ($selectedStatus == 'En cours') ? 'selected' : '' }}>En cours</option>
									<option value="Clos" {{ ($selectedStatus == 'Clos') ? 'selected' : '' }}>Clos</option>
							</select>
						</div>
						@if ($errors->has('status'))
							@foreach ($errors->get('status') as $error)
								<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
						</div>
					@else
						<input type='hidden' name='status' id='status' value="{{$Demand->status or 'A traiter'}}" />
					@endif
					
					@if (session('user')->is_admin_deca == 1)
						<div class="row form-group">
							<label class="col-sm-4 col-md-3 control-label">Moyen de contact *</label>
							<div class="col-sm-8 col-md-5">
								<input type="text" readonly class="form-control"  name="contact_medium" id="contact_medium" value="{{$Demand->contact_medium or 'Portail'}}" />
							</div>
						</div>
					@else
						<input type='hidden' name='contact_medium' id='contact_medium' value="{{$Demand->contact_medium or 'Portail'}}" />
					@endif
					
					@if (session('user')->is_admin_deca == 1)
					<div class="row form-group">
						<label class="col-sm-4 col-md-3 control-label">Priorit&eacute;</label>
						<div class="col-sm-8 col-md-5">
							<select class="form-control" name="priority" id="priority">
								<option value="">-- S&eacute;lectionner une priorit&eacute; --</option>
									<?php $selectedPriority = '';
										if (isset($Demand->priority)) {
											$selectedPriority = $Demand->priority;
										}
										else {
											$selectedPriority = "Basse";
										}
									?>
									<option value="Basse" {{ ($selectedPriority == 'Basse') ? 'selected' : '' }}>Basse</option>
									<option value="Moyenne" {{ ($selectedPriority  == 'Moyenne') ? 'selected' : '' }}>Moyenne</option>
									<option value="Haute" {{ ($selectedPriority  == 'Haute') ? 'selected' : '' }}>Haute</option>
							</select>
						</div>
					</div>
					@else
						<input type='hidden' name='priority' id='priority' value="{{$Demand->priority or 'Basse'}}" />
					@endif
						
					<div class="row form-group">
						<label class="col-sm-4 col-md-3 control-label">Objet</label>
						<div class="col-sm-8 col-md-6">
							<input type='text' readonly class="form-control" name='objectLbl' id='objectLbl' value="{{$Demand->objectLbl or ''}}" /> 
						</div>
					</div>
					<div class="row form-group">
						<label class="col-sm-4 col-md-3 control-label">Motif</label>
						<div class="col-sm-8 col-md-6">
							<input type='text' readonly class="form-control" name='motiveLbl' id='motiveLbl' value="{{$Demand->motiveLbl or ''}}" /> 
						</div>
					</div>	
					<div class="row form-group">
						<label class="col-sm-3 col-md-3 control-label">Demande</label>
						<div class="col-sm-8 col-md-9">
							<textarea readonly rows=8 maxlength=500 class="form-control"  name="comments" id="comments">{{$Demand->comments or ''}}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-folder"></i>
					<span>Pi&egrave;ces jointes</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<table id="tablePJ" class="table table-hover">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Date d'envoi</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td><button type="button" value="" class="deletePJ"><i class="fa fa-trash"></button></td>
						</tr>
					</tbody>
				</table>	
				<button type="button" id="addPJ" class="btn btn-primary">
					<span>
						<i class="fa fa-folder"></i>
						Nouvelle pi&egrave;ce jointe
					</span>
				</button>
			</div>
		</div>
	</div>
	<!--
	<div class="row form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		{!! Form::button('<i class="fa fa-times-circle"></i> Annuler', ['class'=>'btn btn-danger']) !!}
		{!! Form::button('<i class="fa fa-floppy-o"></i> '. $submit_text, ['type' => 'submit', 'class'=>'btn btn-success']) !!}
	</div>
	-->
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil"></i>
					<span>R&eacute;ponses</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<table id="tableComments" class="table table-hover">
					<thead>
						<tr>
							<th>Date</th>
							<th>Personne</th>
							<th>Commentaires</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $Notes as $Note )
							<tr>
								<td>
									<?php $CreatedDate = '';
										if (isset($Note->CreatedDate)) {
											$aCreatedDate = explode("T", $Note->CreatedDate);
											$CreatedDate = date_format(date_create($aCreatedDate[0]), 'd/m/Y');
										}
									?>
									{{ $CreatedDate or ' ' }}
								</td>
								<td></td>
								<td>{{ $Note->Body or ' ' }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<button type="button" id="addComment" class="btn btn-primary">
					<span>
						<i class="fa fa-pencil"></i>
						Nouveau commentaire
					</span>
				</button>
			</div>
		</div>
	</div>
</div>