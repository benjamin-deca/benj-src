<!-- Boite de dialogue "Ajouter un commentaire" -->
<div id="dialogPwd" class="modal-dialog">
	<div class="modal-content">     
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Ajouter un commentaire</h4>
		</div>
		<div class="modal-body">
			<form>
				{!! csrf_field() !!}
				<input type="hidden" class="form-control"  name="external_id" id="external_id" value="" />
				@if (isset($_POST['comment']))
				<div class="row form-group has-error">
					@foreach ($_POST['comment'] as $error)
						<small class="help-block col-sm-10">{{ $error }}</small>
						<br>
					@endforeach
				</div>
				@endif
				<fieldset>
					@if (isset($_POST['comment']))
					<div class="row form-group has-error">
					@else
					<div class="row form-group">
					@endif
						<!--<label class="col-sm-4 control-label">Commentaire *</label>-->
						<div class="col-sm-12">
							<textarea rows="10" cols="80" class="form-control" name="comment" id="comment" />
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="modal-footer">
			<button id="submitComment" type="button" class="btn btn-primary">Envoyer</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>

<!-- Boite de dialogue "Confirmation mot de passe modifié" -->
<div id="dialogPwdOk" class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Ajouter un commentaire</h4>
		</div>
		<div class="modal-body">
			<p>
				Votre réponse a bien &eacute;t&eacute; envoy&eacute;e. Une notification a &eacute;t&eacute; envoy&eacute;e à votre adresse email.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
		</div>
	</div>
</div>