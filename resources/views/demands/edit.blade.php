@extends('layout')

@section('content')
<div id="dialogForm" class="modal fade" role="dialog"></div>
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="demands">Mes demandes</a></li>
			<li><a href="demands/{{$Demand->external_id or ''}}/edit">Demande n&deg; {{$Demand->external_id or ''}}</a></li>
		</ol>
	</div>
</div>

{!! Form::model($Demand, ['method' => 'PATCH', 'route' => ['demands.update', $Demand->external_id]]) !!}
	{!! csrf_field() !!}
	@include('demands/partials/_form', ['submit_text' => 'Enregistrer'])
{!! Form::close() !!}

<script type="text/javascript">
$(document).ready(function() {
	$("#addComment").on("click", function() {
		$('#dialogForm').load('addComment #dialogPwd', function(responseTxt, statusTxt, xhr){
			$('#dialogForm').modal('show');
			$.getScript('js/addComment.js');
			$('#external_id').val() = {{$Demand->external_id or ''}};
		});
	});
});
</script>

@endsection