@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="demands">Mes demandes</a></li>
			<li><a href="demands">Toutes mes demandes</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Mes demandes</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				@if (Session::has('message'))		
					<div class="flash alert-info">			
						<p>{{ Session::get('message') }}</p>		
					</div>	
				@endif
				<button type="button" onclick='window.location.href="{{ route('demands.create') }}";' class="btn btn-primary" id="newCase">
					<span>
						<i class="fa fa-pencil-square-o"></i>
						Nouvelle demande
					</span>
				</button>
				@if ( false )
					<br /><br />Vous n'avez effectu&eacute; aucune demande. Veuillez cliquer sur le bouton "Nouvelle demande" pour initier une nouvelle demande.
				@else
					<table id="tableCase" class="table table-striped table-hover table-heading table-datatable dataTable">
						<thead>
							<tr>
								<th>N&ordm;</th>
								<th>Date de cr&eacute;ation</th>
								<th>Moyen de contact</th>
								<th>Objet</th>
								<th>Motif</th>
								<th>Statut</th>
								<th>Date de cl&ocirc;ture</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach( $Demands as $Demand )
							@if($Demand->Status == 'Clos')
								<tr style="background-color:limegreen">
							@else
								<tr>
							@endif
								<td><a href='demands/{{ $Demand->external_id or '' }}/edit'>{{ $Demand->external_id or '' }}</a></td>
								<td><?php $dateArray = explode("T", $Demand->created_date); 
									$date = date_format(date_create($dateArray[0]), 'd/m/Y');?>
									{{ $date or ' ' }}</td>
								<td>{{ $Demand->contact_medium or ' ' }}</td>
								<td>{{ $Demand->objectLbl or ' ' }}</td>
								<td>{{ $Demand->motiveLbl or ' ' }}</td>
								<td>{{ $Demand->status or ' ' }}</td>
								<td>
									<?php $dateClos = '';
										if (isset($Demand->closed_date)) {
											$dateClosArray = explode("T", $Demand->closed_date);
											$dateClos = date_format(date_create($dateClosArray[0]), 'd/m/Y');
										}?>
									{{ $dateClos or ' ' }}
								</td>
								<td>
									{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('demands.destroy', $Demand->external_id))) !!}
									{!! Form::button('&nbsp;<i class="fa fa-eye"></i>&nbsp;' , ['title' => 'Visualiser', 'class' => 'btn btn-success editCaseLink', 'value' => $Demand->external_id]) !!}
									@if (session('user')->is_admin_deca == 1)
										{!! Form::button('&nbsp;<i class="fa fa-trash"></i>&nbsp;' , ['title' => 'Supprimer','type' => 'submit', 'class' => 'btn btn-danger']) !!}
									@endif
									{!! Form::close() !!}
								</td>
							</tr>
						@endforeach 
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#tableCase').dataTable( {
		language: {
			processing:     "Traitement en cours...",
			search:         "",
			lengthMenu:     "Afficher _MENU_",
			info:           "Demandes _START_ &agrave; _END_ sur _TOTAL_",
			infoEmpty:      "",
			infoFiltered:   "(filtr&eacute; de _MAX_ demandes au total)",
			infoPostFix:    "",
			loadingRecords: "Chargement en cours...",
			zeroRecords:    "Aucune demande &agrave; afficher",
			emptyTable:     "Aucune demande &agrave; afficher",
			paginate: {
				first:      "Premier",
				previous:   "Pr&eacute;c&eacute;dent",
				next:       "Suivant",
				last:       "Dernier"
			},
			aria: {
				sortAscending:  ": activer pour trier la colonne par ordre croissant",
				sortDescending: ": activer pour trier la colonne par ordre décroissant"
			}
		},
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
	} );
	$('#tableCase_filter input').attr("placeholder", "Rechercher");
	$('#tableCase_filter input').attr("class", "");
	
	$(".editCaseLink").on("click", function(){
		var spinner = new Spinner(opts).spin(target);
		var demand = $(".editCaseLink");
		var url = 'demands/' + demand.val() + '/edit';
		alert(url);
		window.location = url;
	});
});
</script>

@endsection