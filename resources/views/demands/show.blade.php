@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="demands">Mes demandes</a></li>
			<li><a href="demands/{{$demand->external_id or ''}}">Demande N�{{$demand->external_id or ''}}</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-7">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-home"></i>
					<span>Mes demandes</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Demande N�{{$demand->external_id or ''}}</h4>
				<div class="form-group">
					<label class="col-sm-3 control-label">Date de cr�ation</label>
					<div class="col-sm-5">
						{{$demand->created_at or ''}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Moyen de contact</label>
					<div class="col-sm-5">
						{{$demand->contact_medium or ''}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Statut</label>
					<div class="col-sm-5">
						{{$demand->status or ''}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Objet</label>
					<div class="col-sm-5">
						{{$demand->object_id or ''}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Motif</label>
					<div class="col-sm-5">
						{{$demand->motive_id or ''}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Commentaires</label>
					<div class="col-sm-5">
						{{$demand->comments or ''}}
					</div>
				</div>
			</div>
		</div>
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-home"></i>
					<span>Mes r&eacute;ponses</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="row form-group">
					<table id="tableComments" class="table table-hover">
						<thead>
						<tr>
							<th>Personne</th>
							<th>Date d'envoi</th>
							<th>R&eacute;ponse</th>
						</tr>
						</thead>
						<tbody>
							@foreach($caseComments->records as $record)
							<tr>
								<th></th>
								<th></th>
								<th></th>
							</tr>
							@endforeach
						</tbody>
					</table>	
					<button type="button" id="add-comments" class="btn btn-primary">Nouvelle r&eacute;ponse</button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-5">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Pi&eagrave;ces compl&eacute;mentaires</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<table id="tablePJ" class="table table-hover">
					<thead>
					<tr>
						<th>Nom</th>
						<th>Date d'envoi</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
						@foreach($casePJ->records as $record)
						<tr>
							<th></th>
							<th></th>
							<th><button type="button" value="" class="deleteCasePJ"><i class="fa fa-trash"></button></th>
						</tr>
						@endforeach
					</tbody>
				</table>	
				<button type="button" id="add-casePJ" class="btn btn-primary">Nouvelle pi&eagrave;ce</button>
			</div>
		</div>
</div>
@stop