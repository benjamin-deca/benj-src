@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="demands">Mes demandes</a></li>
			<li><a href="demands/create">Nouvelle demande</a></li>
		</ol>
	</div>
</div>

{!! Form::model(new App\Demand, ['route' => ['demands.store']]) !!}
	{!! csrf_field() !!}
    <div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Demande</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{$Demand->object or 'Nouvelle demande'}}</h4>
					<input type='hidden' name='portal_id' id='portal_id' value="{{$Demand->portal_id or ''}}" />
					@if (session('user')->is_admin_deca == 1)
						@if ($errors->has('contact_id'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
						<label class="col-sm-4 col-md-3 control-label">Customer Id *</label>
						<div class="col-sm-8 col-md-5">
								<input type="text" class="form-control"  name="contact_id" id="contact_id" value="{{$Demand->contact_id or session('user')->externalcrm_id}}" />
						</div>
						@if ($errors->has('contact_id'))
							@foreach ($errors->get('contact_id') as $error)
								<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
						</div>
					@else
						<input type='hidden' name='contact_id' id='contact_id' value="{{$Demand->contact_id or session('user')->externalcrm_id}}" />
					@endif
					@if (isset($Demand->external_id))
						@if ($errors->has('external_id'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
							<label class="col-sm-4 col-md-3 control-label">N&deg; demande</label>
							<div class="col-sm-8 col-md-5">
								@if (session('user')->is_admin_deca == 1)
									<input type="text" class="form-control"  name="external_id" id="external_id" value="{{$Demand->external_id or ''}}" />
								@else
									{{$Demand->external_id or ''}}
								@endif
							</div>
							@if ($errors->has('external_id'))
								@foreach ($errors->get('external_id') as $error)
									<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
								@endforeach
							@endif
						</div>
					@endif
					
					@if (session('user')->is_admin_deca == 1)
						@if ($errors->has('status'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
						<label class="col-sm-4 col-md-3 control-label">Statut *</label>
						<div class="col-sm-8 col-md-5">
							<select class="form-control" name="status" id="status">
								<option value="">-- S&eacute;lectionner un statut --</option>
									<?php $selectedStatus = '';
										if (isset($Demand->status)) {
											$selectedStatus = $Demand->status;
										}
										else {
											$selectedStatus = "A traiter";
										}
									?>
									<option value="A traiter" {{ ($selectedStatus == 'A traiter') ? 'selected' : '' }}>A traiter</option>
									<option value="En cours" {{ ($selectedStatus == 'En cours') ? 'selected' : '' }}>En cours</option>
									<option value="Clos" {{ ($selectedStatus == 'Clos') ? 'selected' : '' }}>Clos</option>
							</select>
						</div>
						@if ($errors->has('status'))
							@foreach ($errors->get('status') as $error)
								<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
						</div>
					@else
						<input type='hidden' name='status' id='status' value="{{$Demand->status or 'A traiter'}}" />
					@endif
					
					
					@if (session('user')->is_admin_deca == 1)
						@if ($errors->has('contact_medium'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
						<label class="col-sm-4 col-md-3 control-label">Moyen de contact *</label>
						<div class="col-sm-8 col-md-5">
								<input type="text" class="form-control"  name="contact_medium" id="contact_medium" value="{{$Demand->contact_medium or 'Portail'}}" />
						</div>
						@if ($errors->has('contact_medium'))
							@foreach ($errors->get('contact_medium') as $error)
								<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
						</div>
					@else
						<input type='hidden' name='contact_medium' id='contact_medium' value="{{$Demand->contact_medium or 'Portail'}}" />
					@endif
					
					@if (session('user')->is_admin_deca == 1)
					<div class="row form-group">
						<label class="col-sm-4 col-md-3 control-label">Priorit&eacute;</label>
						<div class="col-sm-8 col-md-5">
							<select class="form-control" name="priority" id="priority">
								<option value="">-- S&eacute;lectionner une priorit&eacute; --</option>
									<?php $selectedPriority = '';
										if (isset($Demand->priority)) {
											$selectedPriority = $Demand->priority;
										}
										else {
											$selectedPriority = "Basse";
										}
									?>
									<option value="Basse" {{ ($selectedPriority == 'Basse') ? 'selected' : '' }}>Basse</option>
									<option value="Moyenne" {{ ($selectedPriority  == 'Moyenne') ? 'selected' : '' }}>Moyenne</option>
									<option value="Haute" {{ ($selectedPriority  == 'Haute') ? 'selected' : '' }}>Haute</option>
							</select>
						</div>
					</div>
					@else
						<input type='hidden' name='priority' id='priority' value="{{$Demand->priority or 'Basse'}}" />
					@endif
						
					<div class="row form-group">
						<label class="col-sm-4 col-md-3 control-label">Objet *</label>
						<div class="col-sm-8 col-md-6">
							<select class="populate placeholder" name="object_id" id="object_id">
								<option value="">-- S&eacute;lectionner un objet --</option>
								@foreach($caseObjects as $objet)
									<option value="{{ $objet->id }}">{{ $objet->object }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row form-group">
						<label class="col-sm-4 col-md-3 control-label">Motif *</label>
						<div class="col-sm-8 col-md-6">
							<select class="populate placeholder" name="motive_id" id="motive_id">
								<option value="">-- S&eacute;lectionner un motif --</option>
							</select>
						</div>
					</div>	
					<div class="row form-group">
						<label class="col-sm-3 control-label">Notification SMS</label>
						<div class="col-sm-8">
							<div class="toggle-switch toggle-switch-success">
								<label>
									<input type="checkbox" name="smsNotification" id="smsNotification" value="{{ $Demand->smsNotification or '0'}}" {{ isset($demand->smsNotification) ? 'checked' : '' }} />
									<div class="toggle-switch-inner"></div>
									<div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
								</label>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<label class="col-sm-3 col-md-3 control-label">Commentaires *</label>
						<div class="col-sm-8 col-md-9">
							<textarea rows=8 maxlength=500 class="form-control"  name="comments" id="comments">{{$Demand->comments or ''}}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Mes coordonn&eacute;es</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{ $account[0]->Salutation or ' ' }} {{ $account[0]->FirstName . " " . $account[0]->LastName }}</h4>
					<div class="row form-group">
						<label class="col-sm-3 control-label">T&eacute;l&eacute;phone</label>
						<div class="col-sm-5">
							{{ $account[0]->Phone or 'non renseign&eacute;' }}
						</div>
					</div>
					<div class="row form-group">
						<label class="col-sm-3 control-label">T&eacute;l&eacute;phone mobile</label>
						<div class="col-sm-5">
							{{ $account[0]->MobilePhone or 'non renseign&eacute;' }}
						</div>
					</div>
					<div class="row form-group">
						<label class="col-sm-3 control-label">Email</label>
						<div class="col-sm-5">
							{{ $account[0]->Email }}
						</div>
					</div>
				</div>
				<div class="card address">
					<h4 class="page-header">Votre adresse</h4>
					<p>
						<span>{{ $account[0]->Adresse__c or ' ' }}</span> <br>
						<span>{{ $account[0]->Code_Postal__c or ' '}}</span>
						<span>{{ $account[0]->Ville__c or ' '}}</span>
						<span>{{ $account[0]->Pays__c or ' '}}</span>
					</p>
				</div>
				<div class="row form-group col-xs-12 col-sm-12 col-md-12">
					<button type="button" id="modify-usercase" class="btn btn-primary" id="modifyCoordCase" onclick='window.location.href="account";'>
						<span>
							<i class="fa fa-pencil-square-o"></i>
							Modifier mes coordonn&eacute;es
						</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		{!! Form::button('<i class="fa fa-times-circle"></i> Annuler', ['class'=>'btn btn-danger']) !!}
		{!! Form::button('<i class="fa fa-floppy-o"></i> Enregistrer' , ['type' => 'submit', 'class'=>'btn btn-success']) !!}
	</div>
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#motive_id').select2();
	$('#object_id').select2();
}

$(document).ready(function() {
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
	
	//DialogCase();
	
	$("#object_id").change(function() {
		var $motifs = $("#motive_id");
		$motifs.empty();
		$motifs.append('<option value="" selected="selected">-- S&eacute;lectionner un motif --</option>');
		$motifs.select2();
		
		$.getJSON("getMotifsFromObject/" + $("#object_id").val(), function(data) {
			$.each(data, function(index, value) {
				$motifs.append('<option value="' + index +'">' + value + '</option>');
			});
		});
	});
});
</script>
{!! Form::close() !!}

@endsection