@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="councils">Mairies</a></li>
		</ol>
	</div>
</div>
 
{!! Form::model(new App\Council, ['route' => ['councils.store']]) !!}
	{!! csrf_field() !!}
    @include('councils/partials/_form', ['submit_text' => 'Sauvegarder'])
{!! Form::close() !!}

@endsection