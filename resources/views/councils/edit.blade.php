@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="councils">Mairies</a></li>
		</ol>
	</div>
</div>

{!! Form::model($council, ['onsubmit' => 'return checkOpenHours();' , 'method' => 'PATCH', 'route' => ['councils.update', $council->id]]) !!}
	{!! csrf_field() !!}
	@include('councils/partials/_form', ['submit_text' => 'Sauvegarder'])
{!! Form::close() !!}

<script type="text/javascript">
function checkOpenHours()
{
	var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
	var fg = 0;

	for (var cpt = 0; cpt < days.length; cpt++) 
	{
		var AmOpen = $('#' + days[cpt] + 'AmOpen').val();
		var AmClose = $('#' + days[cpt] + 'AmClose').val();
		var PmOpen = $('#' + days[cpt] + 'PmOpen').val();
		var PmClose = $('#' + days[cpt] + 'PmClose').val();

		if ((AmOpen && !AmClose) || (AmClose && !AmOpen))
		{
			$('#' + days[cpt] + 'AmOpen').css({ "border-color" : "red", "border-width" : "thin" });
			$('#' + days[cpt] + 'AmClose').css({ "border-color" : "red", "border-width" : "thin" });

			fg++;
		}

		if ((PmOpen && !PmClose) || (PmClose && !PmOpen))
		{
			$('#' + days[cpt] + 'PmOpen').css({ "border-color" : "red", "border-width" : "thin" });
			$('#' + days[cpt] + 'PmClose').css({ "border-color" : "red", "border-width" : "thin" });

			fg++;
		}
	}

	if (fg == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
</script>
@endsection