<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-home"></i>
					<span>Mairie</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{$council->fullname or 'Nouvelle mairie'}}</h4>
					<p>
					@if ($errors->has('fullname'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Mairie *</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="fullname" id="fullname" value="{{$council->fullname or ''}}" />
							</div>
					@if ($errors->has('fullname'))
						@foreach ($errors->get('fullname') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					@if ($errors->has('landlinephone'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">T&eacute;l&eacute;phone</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="landlinephone" id="landlinephone" value="{{$council->landlinephone or ''}}" />
							</div>
					@if ($errors->has('landlinephone'))
						@foreach ($errors->get('landlinephone') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					@if ($errors->has('email'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="email" id="email" value="{{$council->email or ''}}" />
							</div>
					@if ($errors->has('email'))
						@foreach ($errors->get('email') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					@if ($errors->has('website'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Site internet</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="website" id="website" value="{{$council->website or ''}}" />
							</div>
					@if ($errors->has('website'))
						@foreach ($errors->get('website') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					</p>
				</div>
				<div class="card address">
					<h4 class="page-header">Adresse de la mairie</h4>
					<p>
					@if ($errors->has('address'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Adresse *</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="address" id="address" value="{{$council->address or ''}}" />
							</div>
					@if ($errors->has('address'))
						@foreach ($errors->get('address') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					@if ($errors->has('zipcode'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Code postal *</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="zipcode" id="zipcode" value="{{$council->zipcode or ''}}" />
							</div>
					@if ($errors->has('zipcode'))
						@foreach ($errors->get('zipcode') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					@if ($errors->has('city'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Ville *</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="city" id="city" value="{{$council->city or ''}}" />
							</div>
					@if ($errors->has('city'))
						@foreach ($errors->get('city') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					@if ($errors->has('country'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Pays *</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="country" id="country" value="{{$council->country or 'FRANCE'}}" />
							</div>
					@if ($errors->has('country'))
						@foreach ($errors->get('country') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					</p>
				</div>
			</div>
		</div>
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Maire</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{$council->mayorName or ''}}</h4>
					<p>
					@if ($errors->has('mayorName'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Maire</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="mayorName" id="mayorName" value="{{$council->mayorName or ''}}" />
							</div>
					@if ($errors->has('mayorName'))
						@foreach ($errors->get('mayorName') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif					
						</div>
					@if ($errors->has('mayorPhone'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">T&eacute;l&eacute;phone</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="mayorPhone" id="mayorPhone" value="{{$council->mayorPhone or ''}}" />
							</div>
					@if ($errors->has('mayorPhone'))
						@foreach ($errors->get('mayorPhone') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif					
						</div>
					@if ($errors->has('mayorMobilePhone'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Portable</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="mayorMobilePhone" id="mayorMobilePhone" value="{{$council->mayorMobilePhone or ''}}" />
							</div>
					@if ($errors->has('mayorMobilePhone'))
						@foreach ($errors->get('mayorMobilePhone') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif					
						</div>
					@if ($errors->has('mayorEmail'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="mayorEmail" id="mayorEmail" value="{{$council->mayorEmail or ''}}" />
							</div>
					@if ($errors->has('mayorEmail'))
						@foreach ($errors->get('mayorEmail') as $error)
							<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
						@endforeach
					@endif
						</div>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-clock-o"></i>
					<span>Horaires d'ouverture</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="open">
					<p>
						<table class="table table-bordered">
							<thead>
								<th class="primary" style="text-align:center">Jours</th>
								<th class="primary" style="text-align:center" colspan="4">Horaires</th>
							</thead>
							<tbody>
								<tr>
									<td class="primary">Lundi</td>
									<td class="row form-group has-error"><input type="time" name="mondayAmOpen" id="mondayAmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->mondayAmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="mondayAmClose" id="mondayAmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->mondayAmClose, 0, 5) ?>" /></td>
									<td><input type="time" name="mondayPmOpen" id="mondayPmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->mondayPmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="mondayPmClose" id="mondayPmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->mondayPmClose, 0, 5) ?>" /></td>
								</tr>
								<tr>
									<td class="primary">Mardi</td>
									<td><input type="time" name="tuesdayAmOpen" id="tuesdayAmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->tuesdayAmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="tuesdayAmClose" id="tuesdayAmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->tuesdayAmClose, 0, 5) ?>" /></td>
									<td><input type="time" name="tuesdayPmOpen" id="tuesdayPmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->tuesdayPmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="tuesdayPmClose" id="tuesdayPmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->tuesdayPmClose, 0, 5) ?>" /></td>
								</tr>
								<tr>
									<td class="primary">Mercredi</td>
									<td><input type="time" name="wednesdayAmOpen" id="wednesdayAmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->wednesdayAmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="wednesdayAmClose" id="wednesdayAmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->wednesdayAmClose, 0, 5) ?>" /></td>
									<td><input type="time" name="wednesdayPmOpen" id="wednesdayPmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->wednesdayPmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="wednesdayPmClose" id="wednesdayPmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->wednesdayPmClose, 0, 5) ?>" /></td>								</tr>
								<tr>
									<td class="primary">Jeudi</td>
									<td><input type="time" name="thursdayAmOpen" id="thursdayAmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->thursdayAmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="thursdayAmClose" id="thursdayAmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->thursdayAmClose, 0, 5) ?>" /></td>
									<td><input type="time" name="thursdayPmOpen" id="thursdayPmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->thursdayPmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="thursdayPmClose" id="thursdayPmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->thursdayPmClose, 0, 5) ?>" /></td>								</tr>
								<tr>
									<td class="primary">Vendredi</td>
									<td><input type="time" name="fridayAmOpen" id="fridayAmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->fridayAmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="fridayAmClose" id="fridayAmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->fridayAmClose, 0, 5) ?>" /></td>
									<td><input type="time" name="fridayPmOpen" id="fridayPmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->fridayPmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="fridayPmClose" id="fridayPmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->fridayPmClose, 0, 5) ?>" /></td>								</tr>
								<tr>
									<td class="primary">Samedi</td>
									<td><input type="time" name="saturdayAmOpen" id="saturdayAmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->saturdayAmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="saturdayAmClose" id="saturdayAmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->saturdayAmClose, 0, 5) ?>" /></td>
									<td><input type="time" name="saturdayPmOpen" id="saturdayPmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->saturdayPmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="saturdayPmClose" id="saturdayPmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->saturdayPmClose, 0, 5) ?>" /></td>								</tr>
								<tr>
									<td class="primary">Dimanche</td>
									<td><input type="time" name="sundayAmOpen" id="sundayAmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->sundayAmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="sundayAmClose" id="sundayAmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->sundayAmClose, 0, 5) ?>" /></td>
									<td><input type="time" name="sundayPmOpen" id="sundayPmOpen" style="height:26px;width:80px" value="<?php echo substr($council->openHours->sundayPmOpen, 0, 5) ?>" /></td>
									<td><input type="time" name="sundayPmClose" id="sundayPmClose" style="height:26px;width:80px" value="<?php echo substr($council->openHours->sundayPmClose, 0, 5) ?>" /></td>			
								</tr>
							</tbody>
						</table>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		{!! Form::submit($submit_text, ['id' => 'submitCouncil', 'class' => 'btn btn-primary']) !!}
	</div>
</div>	

