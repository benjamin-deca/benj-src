@extends('layout')

@section('content')

<style type="text/css">
  html, body, #map-canvas { height: 100%; margin: 0; padding: 0;}
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmKGzyoTvJMdOJ6hC25saSHF2RF-cSyyI">
</script>
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="#">Ma mairie</a></li>
			<li><a href="council">{{ $council->fullname or '' }}</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-home"></i>
					<span>Ma mairie</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{$council->fullname or ''}}</h4>
						<div class="row form-group">
							<label class="col-sm-3 control-label">T&eacute;l&eacute;phone</label>
							<div class="col-sm-5">
								{{$council->landlinephone or ''}}
							</div>
						</div>
						<div class="row form-group">
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-5">
								<a href="mailto:{{$council->email or ''}}">{{$council->email or ''}}</a>
							</div>
						</div>
						<div class="row form-group">
							<label class="col-sm-3 control-label">Site internet</label>
							<div class="col-sm-5">
								{{$council->website or ''}}
							</div>
						</div>
						<button type="button" onclick='window.location.href="demands/create";' class="btn btn-primary" id="contactUs">
							<span>
								<i class="fa fa-pencil-square-o"></i>
								Nous contacter
							</span>
						</button>
				</div>
				<div class="card address">
					<h4 class="page-header">Adresse de la mairie</h4>
					<p>
						<span>{{$council->address or ''}}</span> <br>
						<span>{{$council->zipcode or ''}}</span>
						<span>{{$council->city or ''}}</span>
						<span>{{$council->country or ''}}</span>
					</p>
					<div class="map" id="map-canvas" style="height: 450px;">
					</div>
				</div>
				<br/>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-clock-o"></i>
					<span>Horaires d'ouvertures</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="open">
					<p>
						<table class="table table-bordered">
							<thead>
								<th class="primary" style="text-align:center">Jours</th>
								<th class="primary" style="text-align:center" colspan="2">Horaires</th>
							</thead>
							<tbody>
								<tr>
									<td class="primary">Lundi</td>
									<td id="mondayAm" class="alert-danger" style="text-align:center"></td>
									<td id="mondayPm" class="alert-danger" style="text-align:center"></td>
								</tr>
								<tr>
									<td class="primary">Mardi</td>
									<td id="tuesdayAm" class="alert-danger" style="text-align:center"></td>
									<td id="tuesdayPm" class="alert-danger" style="text-align:center"></td>
								</tr>
								<tr>
									<td class="primary">Mercredi</td>
									<td id="wednesdayAm" class="alert-danger" style="text-align:center"></td>
									<td id="wednesdayPm" class="alert-danger" style="text-align:center"></td>
								</tr>
								<tr>
									<td class="primary">Jeudi</td>
									<td id="thursdayAm" class="alert-danger" style="text-align:center"></td>
									<td id="thursdayPm" class="alert-danger" style="text-align:center"></td>
								</tr>
								<tr>
									<td class="primary">Vendredi</td>
									<td id="fridayAm" class="alert-danger" style="text-align:center"></td>
									<td id="fridayPm" class="alert-danger" style="text-align:center"></td>
								</tr>
								<tr>
									<td class="primary">Samedi</td>
									<td id="saturdayAm" class="alert-danger" style="text-align:center"></td>
									<td id="saturdayPm" class="alert-danger" style="text-align:center"></td>
								</tr>
								<tr>
									<td class="primary">Dimanche</td>
									<td id="sundayAm" class="alert-danger" style="text-align:center"></td>
									<td id="sundayPm" class="alert-danger" style="text-align:center"></td>
								</tr>
							</tbody>
						</table>
					</p>
				</div>
				<div id="divOpenHours" class="flash" style="text-align:center">			
					<p id="pOpenHours"></p>		
				</div>
			</div>
		</div>
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Maire</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">{{$council->mayorName or ''}}</h4>
				<div class="row form-group">
					<label class="col-sm-4 control-label">T&eacute;l&eacute;phone</label>
					<div class="col-sm-4">
						{{$council->mayorPhone or ''}}
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-4 control-label">T&eacute;l&eacute;phone mobile</label>
					<div class="col-sm-4">
						{{$council->mayorMobilePhone or ''}}
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-4 control-label">Email</label>
					<div class="col-sm-4">
						<a href="mailto:{{$council->mayorEmail or ''}}">{{$council->mayorEmail or ''}}</a>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<script>
function initialize() {
var geocoder = new google.maps.Geocoder();
var mapOptions = {
    scaleControl: true,
	zoom: 15,
    center: {lat: -34.397, lng: 150.644}
  };
  
var  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

var address = "{{$council->address or ''}}," + " " + "{{$council->city or ''}}," + " " + "{{$council->zipcode or ''}}";
geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      $('#map-canvas').html("La carte n'a pas pu s'afficher, veuillez v&eacute;rifier l'adresse.");
    }
 });
}
google.maps.event.addDomListener(window, 'load', initialize);

$(document).ready(function() 
{
	/* A OPTIMISER */
	var mondayAmOpen	 = "<?php echo $council->openHours->mondayAmOpen 	 ? $council->openHours->mondayAmOpen		: '' ?>";
	var mondayAmClose	 = "<?php echo $council->openHours->mondayAmClose 	 ? $council->openHours->mondayAmClose	: '' ?>";
    var mondayPmOpen	 = "<?php echo $council->openHours->mondayPmOpen 	 ? $council->openHours->mondayPmOpen		: '' ?>";
    var mondayPmClose	 = "<?php echo $council->openHours->mondayPmClose 	 ? $council->openHours->mondayPmClose	: '' ?>";
    var tuesdayAmOpen	 = "<?php echo $council->openHours->tuesdayAmOpen 	 ? $council->openHours->tuesdayAmOpen	: '' ?>";
    var tuesdayAmClose	 = "<?php echo $council->openHours->tuesdayAmClose 	 ? $council->openHours->tuesdayAmClose	: '' ?>";
    var tuesdayPmOpen	 = "<?php echo $council->openHours->tuesdayPmOpen 	 ? $council->openHours->tuesdayPmOpen	: '' ?>";
    var tuesdayPmClose	 = "<?php echo $council->openHours->tuesdayPmClose 	 ? $council->openHours->tuesdayPmClose	: '' ?>";
    var wednesdayAmOpen	 = "<?php echo $council->openHours->wednesdayAmOpen	 ? $council->openHours->wednesdayAmOpen	: '' ?>";
    var wednesdayAmClose = "<?php echo $council->openHours->wednesdayAmClose ? $council->openHours->wednesdayAmClose	: '' ?>";
    var wednesdayPmOpen	 = "<?php echo $council->openHours->wednesdayPmOpen  ? $council->openHours->wednesdayPmOpen	: '' ?>";
    var wednesdayPmClose = "<?php echo $council->openHours->wednesdayPmClose ? $council->openHours->wednesdayPmClose	: '' ?>";
    var thursdayAmOpen	 = "<?php echo $council->openHours->thursdayAmOpen 	 ? $council->openHours->thursdayAmOpen	: '' ?>";
    var thursdayAmClose	 = "<?php echo $council->openHours->thursdayAmClose  ? $council->openHours->thursdayAmClose	: '' ?>";
    var thursdayPmOpen	 = "<?php echo $council->openHours->thursdayPmOpen 	 ? $council->openHours->thursdayPmOpen	: '' ?>";
    var thursdayPmClose	 = "<?php echo $council->openHours->thursdayPmClose  ? $council->openHours->thursdayPmClose	: '' ?>";
    var fridayAmOpen	 = "<?php echo $council->openHours->fridayAmOpen 	 ? $council->openHours->fridayAmOpen		: '' ?>";
    var fridayAmClose	 = "<?php echo $council->openHours->fridayAmClose 	 ? $council->openHours->fridayAmClose	: '' ?>";
    var fridayPmOpen	 = "<?php echo $council->openHours->fridayPmOpen 	 ? $council->openHours->fridayPmOpen		: '' ?>";
    var fridayPmClose	 = "<?php echo $council->openHours->fridayPmClose 	 ? $council->openHours->fridayPmClose	: '' ?>";
    var saturdayAmOpen	 = "<?php echo $council->openHours->saturdayAmOpen 	 ? $council->openHours->saturdayAmOpen	: '' ?>";
    var saturdayAmClose	 = "<?php echo $council->openHours->saturdayAmClose  ? $council->openHours->saturdayAmClose	: '' ?>";
    var saturdayPmOpen	 = "<?php echo $council->openHours->saturdayPmOpen 	 ? $council->openHours->saturdayPmOpen	: '' ?>";
    var saturdayPmClose	 = "<?php echo $council->openHours->saturdayPmClose  ? $council->openHours->saturdayPmClose	: '' ?>";
    var sundayAmOpen	 = "<?php echo $council->openHours->sundayAmOpen 	 ? $council->openHours->sundayAmOpen   	: '' ?>";
    var sundayAmClose	 = "<?php echo $council->openHours->sundayAmClose 	 ? $council->openHours->sundayAmClose	: '' ?>";
    var sundayPmOpen	 = "<?php echo $council->openHours->sundayPmOpen 	 ? $council->openHours->sundayPmOpen		: '' ?>";
    var sundayPmClose	 = "<?php echo $council->openHours->sundayPmClose 	 ? $council->openHours->sundayPmClose	: '' ?>";
    /**/

    var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

	for (var cpt = 0; cpt < days.length; cpt++) 
	{
		var AmOpen = eval(days[cpt] + "AmOpen").substr(0, 5);
		var AmClose = eval(days[cpt] + "AmClose").substr(0, 5);
		var PmOpen = eval(days[cpt] + "PmOpen").substr(0, 5);
		var PmClose = eval(days[cpt] + "PmClose").substr(0, 5);

		if (AmOpen != '' && AmClose != '')
		{ 
			$('#' + days[cpt] + 'Am').html(AmOpen + " - " + AmClose);
			$('#' + days[cpt] + 'Am').switchClass('alert-danger', 'alert-success'); 
		}

		if (PmOpen != '' && PmClose != '')
		{ 
			$('#' + days[cpt] + 'Pm').html(PmOpen + " - " + PmClose);
			$('#' + days[cpt] + 'Pm').switchClass('alert-danger', 'alert-success'); 
		}

		var today = new Date();
	
		if (cpt == today.getDay())
		{
			var now = today.getTime();
			var year = today.getFullYear();
			var month = today.getMonth();
			var day = today.getDate();

			var dAmOpenA  = AmOpen  ? new Date(year, month, day, AmOpen.split(":")[0] - 1, 	AmOpen.split(":")[1], 	0) : null;
			var dAmOpen   = AmOpen 	? new Date(year, month, day, AmOpen.split(":")[0], 		AmOpen.split(":")[1], 	0) : null;
			var dAmCloseA = AmClose ? new Date(year, month, day, AmClose.split(":")[0] - 1, AmClose.split(":")[1], 	0) : null;
			var dAmClose  = AmClose ? new Date(year, month, day, AmClose.split(":")[0], 	AmClose.split(":")[1], 	0) : null;
			var dPmOpenA  = PmOpen 	? new Date(year, month, day, PmOpen.split(":")[0] - 1, 	PmOpen.split(":")[1], 	0) : null;
			var dPmOpen   = PmOpen 	? new Date(year, month, day, PmOpen.split(":")[0], 		PmOpen.split(":")[1], 	0) : null;
			var dPmCloseA = PmClose ? new Date(year, month, day, PmClose.split(":")[0] - 1, PmClose.split(":")[1], 	0) : null;
			var dPmClose  = PmClose ? new Date(year, month, day, PmClose.split(":")[0], 	PmClose.split(":")[1],	0) : null;

			if ((dAmOpen && now >= dAmOpen.getTime() && dAmCloseA && now < dAmCloseA.getTime()) || 
				(dPmOpen && now >= dPmOpen.getTime() && dPmCloseA && now < dPmCloseA.getTime()))
			{
				$('#divOpenHours').addClass('alert-success'); 
				$('#pOpenHours').html("Actuellement Ouverte");
			}
			else if ((dAmOpen && now >= dAmOpenA.getTime() && now < dAmOpen.getTime()) || 
					 (dPmOpen && now >= dPmOpenA.getTime() && now < dPmOpen.getTime())) 
			{
				var delay = (now < dAmOpen.getTime()) ? dAmOpen.getTime() - now : dPmOpen.getTime() - now;

				$('#divOpenHours').addClass('alert-warning'); 
				$('#pOpenHours').html("Ouvre dans " + (new Date(delay).getMinutes() + 1) + " minute(s)");
			}
			else if ((dAmClose && now >= dAmCloseA.getTime() && now < dAmClose.getTime()) ||
					 (dPmClose && now >= dPmCloseA.getTime() && now < dPmClose.getTime()))
			{
				var delay = (now < dAmClose.getTime()) ? dAmClose.getTime() - now : dPmClose.getTime() - now;
	
				$('#divOpenHours').addClass('alert-warning'); 
				$('#pOpenHours').html("Ferme dans " + (new Date(delay).getMinutes() + 1) + " minute(s)");
			}
			else
			{
				$('#divOpenHours').addClass('alert-danger'); 
				$('#pOpenHours').html("Actuellement Fermée");
			}
		}
	}
});
</script>
@stop