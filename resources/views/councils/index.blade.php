@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="councils">Mairies</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-home"></i>
					<span>Mairie</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				@if (Session::has('message'))		
					<div class="flash alert-info">			
						<p>{{ Session::get('message') }}</p>		
					</div>	
				@endif
				@if (session('user')->is_admin_deca == 1)
				<center><button type="button" onclick='window.location.href="{{ route('councils.create') }}";' class="btn btn-primary btn-sm btn-label-left" id="newCouncil"><span><i class="fa fa-pencil-square-o"></i></span>Nouvelle mairie</button></center>
				@endif
				@if ( !$councils->count() )
					Aucune mairie recens&eacute;e. Veuillez cliquer sur le bouton "Nouvelle mairie" pour cr&eacute;er une mairie.
				@else
					<table id="tableCouncil" class="table table-striped table-hover table-heading table-datatable dataTable">
						<thead>
							<tr>
								<th></th>
								<th>Nom</th>
								<th>Adresse</th>
								<th>Code postal</th>
								<th>Ville</th>
								<th>Maire</th>
							</tr>
						</thead>
						<tbody>
						@foreach( $councils as $council )
							<tr>
								<td>
									{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('councils.destroy', $council->id))) !!}
									{!! link_to_route('councils.edit', 'Modifier', array($council->id), array('class' => 'btn btn-info')) !!}
									@if (session('user')->is_admin_deca == 1)
									{!! Form::submit('Supprimer', array('class' => 'btn btn-danger')) !!}
									@endif
									{!! Form::close() !!}
								</td>
								<td>{{ $council->fullname or ''}}</td>
								<td>{{ $council->address or '' }}</td>
								<td>{{ $council->zipcode or '' }}</td>
								<td>{{ $council->city or '' }}</td>
								<td>{{ $council->mayorName or '' }}</td>
							</tr>
						@endforeach 
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#tableCouncil').dataTable( {
		language: {
			processing:     "Traitement en cours...",
			search:         "",
			lengthMenu:     "Afficher _MENU_",
			info:           "Mairies _START_ &agrave; _END_ sur _TOTAL_",
			infoEmpty:      "",
			infoFiltered:   "(filtr&eacute; de _MAX_ mairies au total)",
			infoPostFix:    "",
			loadingRecords: "Chargement en cours...",
			zeroRecords:    "Aucune mairie &agrave; afficher",
			emptyTable:     "Aucune mairie &agrave; afficher",
			paginate: {
				first:      "Premier",
				previous:   "Pr&eacute;c&eacute;dent",
				next:       "Suivant",
				last:       "Dernier"
			},
			aria: {
				sortAscending:  ": activer pour trier la colonne par ordre croissant",
				sortDescending: ": activer pour trier la colonne par ordre décroissant"
			}
		},
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
	} );
	$('#tableCouncil_filter input').attr("placeholder", "Rechercher");
	$('#tableCouncil_filter input').attr("class", "");
});
</script>

@endsection