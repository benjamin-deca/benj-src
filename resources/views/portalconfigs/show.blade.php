@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="./home">Accueil</a></li>
			<li><a href="./portalConfigs">Configuration</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Configuration</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">Variable</h4>
					<p>
						<div class="form-group">
							<label class="col-sm-3 control-label">Variable</label>
							<div class="col-sm-5">
								{{$portalConfig->config_key or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Valeur</label>
							<div class="col-sm-5">
								{{$portalConfig->config_value or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Libell&eacute;</label>
							<div class="col-sm-5">
								{{$portalConfig->config_label or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Commentaires</label>
							<div class="col-sm-5">
								{{$portalConfig->config_comments or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Marker</label>
							<div class="col-sm-5">
								{{$portalConfig->config_marker or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Variable activ&eacute;e Decacity</label>
							<div class="col-sm-5">
								{{$portalConfig->activated or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Variable Modifiable mairie</label>
							<div class="col-sm-5">
								{{$portalConfig->council_enabled or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Variable activ&eacute;e</label>
							<div class="col-sm-5">
								{{$portalConfig->council_activated or ''}}
							</div>
						</div>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@stop