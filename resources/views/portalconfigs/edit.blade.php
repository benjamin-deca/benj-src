@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="./home">Accueil</a></li>
			<li><a href="./portalConfigs">Configuration</a></li>
		</ol>
	</div>
</div>

{!! Form::model($portalConfig, ['method' => 'PATCH', 'route' => ['portalConfigs.update', $portalConfig->id]]) !!}
	{!! csrf_field() !!}
	@include('portalConfigs/partials/_form', ['submit_text' => 'Enregistrer'])
{!! Form::close() !!}

@endsection