<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Configuration</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">{{$portalConfig->config_label or 'Nouvelle variable'}}</h4>
					<input type='hidden' name='council_id' value='council_id' value="{{$portalConfig->council_id or ''}}" />
					<p>
						@if (session('user')->is_admin_deca == 1)
							@if ($errors->has('config_label'))
								<div class="row form-group has-error">
							@else
								<div class="row form-group">
							@endif
							<label class="col-sm-4 col-md-3 control-label">Libell&eacute; *</label>
							<div class="col-sm-6 col-md-8">
									<input type="text" class="form-control"  name="config_label" id="config_label" value="{{$portalConfig->config_label or ''}}" />
							</div>
							@if ($errors->has('config_label'))
								@foreach ($errors->get('config_label') as $error)
									<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
								@endforeach
							@endif
							</div>
						@else
							<input type='hidden' name='config_label' value='config_label' value="{{$portalConfig->config_label or ''}}" />
						@endif
						@if ($errors->has('config_key'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
							<label class="col-sm-3 control-label">Variable *</label>
							<div class="col-sm-4">
								@if (session('user')->is_admin_deca == 1)
									<input type="text" class="form-control"  name="config_key" id="config_key" value="{{$portalConfig->config_key or ''}}" />
								@else
									{{$portalConfig->config_key or ''}}
								@endif
							</div>
							@if ($errors->has('config_key'))
								@foreach ($errors->get('config_key') as $error)
									<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
								@endforeach
							@endif
						</div>
						@if ($errors->has('config_value'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
							<label class="col-sm-3 control-label">Valeur *</label>
							<div class="col-sm-4">
								<input type="text" class="form-control"  name="config_value" id="config_value" value="{{$portalConfig->config_value or ''}}" />
							</div>
							@if ($errors->has('config_value'))
								@foreach ($errors->get('config_value') as $error)
									<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
								@endforeach
							@endif
						</div>
						@if ($errors->has('config_marker'))
							<div class="row form-group has-error">
						@else
							<div class="row form-group">
						@endif
							<label class="col-sm-3 control-label">Marker *</label>
							<div class="col-sm-2">
								@if (session('user')->is_admin_deca == 1)
									<input type="text" class="form-control"  name="config_marker" id="config_marker" value="{{$portalConfig->config_marker or ''}}" />
								@else
									{{$portalConfig->config_marker or ''}}
								@endif
							</div>
							@if ($errors->has('config_marker'))
								@foreach ($errors->get('config_marker') as $error)
									<small class="help-block col-sm-8 col-sm-offset-3">{{ $error }}</small>
								@endforeach
							@endif
						</div>
						<div class="row form-group">
							<label class="col-sm-3 control-label">Commentaires</label>
							<div class="col-sm-8">
								<textarea rows=4 maxlength=500 class="form-control"  name="config_comments" id="config_comments">{{$portalConfig->config_comments or ''}}</textarea>
							</div>
						</div>
						@if (session('user')->is_admin_deca == 1)
						<div class="row form-group">
							<label class="col-sm-3 control-label">Variable activ&eacute;e Decacity</label>
							<div class="col-sm-5">
								<div class="toggle-switch toggle-switch-success">
									<label>
										<input type="checkbox" name="activated" id="activated" value="{{ $portalConfig->council_activated or '0'}}" {{ isset($portalConfig->activated) ? 'checked' : '' }} />
										<div class="toggle-switch-inner"></div>
										<div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
									</label>
								</div>
							</div>
						</div>
						@else
							<input type='hidden' name='activated' value='activated' value="{{$portalConfig->activated or ''}}" />
						@endif
						@if (session('user')->is_admin_deca == 1)
						<div class="row form-group">
							<label class="col-sm-3 control-label">Variable Modifiable mairie</label>
							<div class="col-sm-5">
								<div class="toggle-switch toggle-switch-success">
									<label>
										<input type="checkbox" name="council_enabled" id="council_enabled" value="{{ $portalConfig->council_activated or '0'}}" {{ isset($portalConfig->council_enabled) ? 'checked' : '' }} />
										<div class="toggle-switch-inner"></div>
										<div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
									</label>
								</div>
							</div>
						</div>
						@else
							<input type='hidden' name='council_enabled' value='council_enabled' value="{{$portalConfig->council_enabled or ''}}" />
						@endif
						<div class="row form-group">
							<label class="col-sm-3 control-label">Variable activ&eacute;e</label>
							<div class="col-sm-5">
								<div class="toggle-switch toggle-switch-success">
									<label>
										<input type="checkbox" name="council_activated" id="council_activated" value="{{ $portalConfig->council_activated or '0'}}" {{ isset($portalConfig->council_activated) ? 'checked' : '' }} />
										<div class="toggle-switch-inner"></div>
										<div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
									</label>
								</div>
							</div>
						</div>
						
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		{!! Form::button("Annuler", ['class'=>'btn btn-primary']) !!}
		{!! Form::submit($submit_text, ['class'=>'btn btn-success']) !!}
	</div>
</div>