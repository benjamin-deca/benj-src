@extends('layout')

@section('content')
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="portalConfigs">Configuration</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Configuration</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				@if (Session::has('message'))		
					<div class="flash alert-info">			
						<p>{{ Session::get('message') }}</p>		
					</div>	
				@endif
				@if (session('user')->is_admin_deca == 1)
				<center><button type="button" onclick='window.location.href="{{ route('portalConfigs.create') }}";' class="btn btn-primary btn-sm btn-label-left" id="newPortalConfig"><span><i class="fa fa-pencil-square-o"></i></span>Nouvelle variable</button></center>
				@endif
				@if ( !$portalConfigs->count() )
					Aucune variable recens&eacute;e. Veuillez cliquer sur le bouton "Nouvelle variable" pour cr&eacute;er une variable.
				@else
					<table id="tableConfig" class="table table-striped table-hover table-heading table-datatable dataTable">
						<thead>
							<tr>
								<th></th>
								<th>Variable</th>
								<th>Valeur</th>
								<th>Active</th>
								<th>Aide</th>
								@if (session('user')->is_admin_deca == 1)
									<th>Marker</th>
									<th>Active Decacity</th>
								@endif
							</tr>
						</thead>
						<tbody>
						@foreach( $portalConfigs as $portalConfig )
							<tr>
								<td>
									{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('portalConfigs.destroy', $portalConfig->id))) !!}
									{!! link_to_route('portalConfigs.edit', 'Modifier', array($portalConfig->id), array('class' => 'btn btn-info')) !!}
									@if (session('user')->is_admin_deca == 1)
										{!! Form::submit('Supprimer', array('class' => 'btn btn-danger')) !!}
									@endif
									{!! Form::close() !!}
								</td>
								<td>{{ $portalConfig->config_key or ''}}</td>
								<td>{{ $portalConfig->config_value or '' }}</td>
								<td>{{ $portalConfig->council_activated or '' }}</td>
								<td><i class="fa fa-comment" title="{{ $portalConfig->config_comments or '' }}"></i></td>
								@if (session('user')->is_admin_deca == 1)
									<td>{{ $portalConfig->config_marker or '' }}</td>
									<td>{{ $portalConfig->activated or '' }}</td>
								@endif
							</tr>
						@endforeach 
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
		$('#tableConfig').dataTable( {
		language: {
			processing:     "Traitement en cours...",
			search:         "",
			lengthMenu:     "Afficher _MENU_",
			info:           "Variables _START_ &agrave; _END_ sur _TOTAL_",
			infoEmpty:      "",
			infoFiltered:   "(filtr&eacute; de _MAX_ variables au total)",
			infoPostFix:    "",
			loadingRecords: "Chargement en cours...",
			zeroRecords:    "Aucune variable &agrave; afficher",
			emptyTable:     "Aucune variable &agrave; afficher",
			paginate: {
				first:      "Premier",
				previous:   "Pr&eacute;c&eacute;dent",
				next:       "Suivant",
				last:       "Dernier"
			},
			aria: {
				sortAscending:  ": activer pour trier la colonne par ordre croissant",
				sortDescending: ": activer pour trier la colonne par ordre décroissant"
			}
		},
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
	} );
	$('#tableConfig_filter input').attr("placeholder", "Rechercher");
	$('#tableConfig_filter input').attr("class", "");
});
</script>

@endsection