<!-- Boite de dialogue "Modifier votre adresse" -->

<div id="dialogAddress" class="modal-dialog">
    <!-- Modal content-->
	<div class="modal-content">     
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modifier votre adresse</h4>
		</div>
		<div class="modal-body">
			<form>
			{!! csrf_field() !!}
				<fieldset>
					@if (isset($_POST['address']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="address" class="col-sm-4 control-label">Adresse</label>
						<div class="col-sm-7">
							<input type="text" name="address" id="address_form" value="" class="form-control">
						</div>
						@if (isset($_POST['address']))
							@foreach ($_POST['address'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					@if (isset($_POST['zipcode']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="zipcode" class="col-sm-4 control-label">Code postal</label>
						<div class="col-sm-7">
							<input type="text" name="zipcode" id="zipcode_form" value="" class="form-control" maxlength="5">
						</div>
						@if (isset($_POST['zipcode']))
							@foreach ($_POST['zipcode'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					@if (isset($_POST['city']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="city" class="col-sm-4 control-label">Ville</label>
						<div class="col-sm-7">
							<input type="text" name="city" id="city_form" value="" class="form-control">
						</div>
						@if (isset($_POST['city']))
							@foreach ($_POST['city'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					<div class="form-group">
						<label for="country" class="col-sm-4 control-label">Pays</label>
						<div class="col-sm-7">
							<input type="text" name="country" id="country_form" value="" class="form-control">
						</div>
					</div>		
				</fieldset>
			</form>
		</div>
		<div class="modal-footer">
			<button id="submitAddress" type="button" class="btn btn-primary">Modifier</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>

<!-- Boite de dialogue "Confirmation adresse" -->

<div id="dialogAddressOk" class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Votre adresse</h4>
		</div>
		<div class="modal-body">
			<p>
				Votre adresse a bien &eacute;t&eacute; modifi&eacute;e.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" onclick='window.location.href="account";'>Ok</button>
		</div>
	</div>
</div>