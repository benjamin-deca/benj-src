<!-- Boite de dialogue "Modifier vos coordonnées" -->

<div id="dialogCoord" class="modal-dialog">
    <!-- Modal content-->
	<div class="modal-content">     
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modifier vos coordonn&eacute;es</h4>
		</div>
		<div class="modal-body">
			<form>
			{!! csrf_field() !!}
				<fieldset>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label">Situation maritale</label>
						<div class="col-sm-7">
							<select class="form-control" name="situation_maritale" id="situation_maritale_form" >
								<option value="">-- S&eacute;lectionner --</option>
								<option>C&eacute;libataire</option>
								<option>Mari&eacute;(e)</option>
								<option>Divorc&eacute;(e)</option>
								<option>Pasc&eacute;(e)</option>
								<option>Veuf(ve)</option>
							</select>
						</div>
					</div>
					<br>
					<div class="form-group">
						<label for="birthdatePicker" class="col-sm-4 control-label">Date de naissance</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="birthdate_form" placeholder="">
						</div>
					</div>
					<br>
					@if (isset($_POST['landlinephone']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="phone" class="col-sm-4 control-label">T&eacute;l&eacute;phone</label>
						<div class="col-sm-7">
							<input type="text" name="phone" id="phone_form" value="" class="form-control" maxlength="10">
						</div>
						@if (isset($_POST['landlinephone']))
							@foreach ($_POST['landlinephone'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					@if (isset($_POST['mobilephone']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="mobilephone" class="col-sm-4 control-label">Portable</label>
						<div class="col-sm-7">
							<input type="text" name="mobilephone" id="mobilephone_form" value="" class="form-control" maxlength="10">
						</div>
						@if (isset($_POST['mobilephone']))
							@foreach ($_POST['mobilephone'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					@if (isset($_POST['email']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="email" class="col-sm-4 control-label">Email</label>
						<div class="col-sm-7">
							<input type="text" name="email" id="email_form" value="" class="form-control">
						</div>
						@if (isset($_POST['email']))
							@foreach ($_POST['email'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
				</fieldset>
			</form>
		</div>
		<div class="modal-footer">
			<button id="submitCoord" type="button" class="btn btn-primary">Modifier</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>

<!-- Boite de dialogue "Confirmation coordonnées -->

<div id="dialogCoordOk" class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Vos coordonn&eacute;es</h4>
		</div>
		<div class="modal-body">
			<p>
				Vos coordonn&eacute;es ont bien &eacute;t&eacute; modifi&eacute;es.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" onclick='window.location.href="account";'>Ok</button>
		</div>
	</div>
</div>