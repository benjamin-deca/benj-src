<!-- Boite de dialogue "Ajouter une association" -->

<div id="dialogAsso" class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Ajouter une association</h4>
		</div>
		<div class="modal-body">
			<form>
			{!! csrf_field() !!}
				<fieldset>
					@if (isset($_POST['id_asso']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="name" class="col-sm-4 control-label">Nom de l'association *</label>
						<div class="col-sm-7">
							<select class="form-control" name="name_asso" id="name_asso_form" >
								<option value="">-- S&eacute;lectionner une association --</option>
							</select>
						</div>
						@if (isset($_POST['id_asso']))
							@foreach ($_POST['id_asso'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					@if (isset($_POST['fonction']))
					<div class="form-group has-error">
					@else
					<div class="form-group">
					@endif
						<label for="name" class="col-sm-4 control-label">Fonction *</label>
						<div class="col-sm-7">
							<select class="form-control" name="fonction_asso" id="fonction_asso_form" >
								<option value="">-- S&eacute;lectionner une fonction --</option>
								<option>Pr&eacute;sident</option>
								<option>Tr&eacute;sorier</option>
								<option>Secr&eacute;taire</option>
								<option>Bureau</option>
								<option>Membre</option>
							</select>
						</div>
						@if (isset($_POST['fonction']))
							@foreach ($_POST['fonction'] as $error)
								<small class="help-block col-sm-9">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<br>
					<div class="form-group">
						<label for="birthdatePicker" class="col-sm-4 control-label">Depuis le</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="since_asso_form" placeholder="">
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="modal-footer">
			<button id="submitAsso" type="button" class="btn btn-primary">Enregistrer</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>

<!-- Boite de dialogue "Association ajoutée" -->

<div id="dialogAssoOk" class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Votre association</h4>
		</div>
		<div class="modal-body">
			<p>
				Votre association a bien &eacute;t&eacute; ajout&eacute;e.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" onclick='window.location.href="account";'>Ok</button>
		</div>
	</div>
</div>