@extends('layout')

@section('content')
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="users">Utilisateurs</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-users"></i>
					<span>Utilisateurs</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				@if (Session::has('message'))		
					<div class="flash alert-info">			
						<p>{{ Session::get('message') }}</p>		
					</div>	
				@endif
				@if (session('user')->is_admin_deca == 1)
					<center><button type="button" onclick='window.location.href="{{ route('users.create') }}";' class="btn btn-primary btn-sm btn-label-left" id="newUser"><span><i class="fa fa-pencil-square-o"></i></span>Nouvel utilisateur</button></center>
				@endif
				@if ( !$users->count() )
					Aucun utilisateur recens&eacute;e. Veuillez cliquer sur le bouton "Nouvel utilisateur" pour cr&eacute;er un compte.
				@else
					<table id="tableUser" class="table table-striped table-hover table-heading table-datatable dataTable">
						<thead>
							<tr>
								<th></th>
								<th>Pr&eacute;nom</th>
								<th>Nom</th>
								<th>Email</th>
								<th>Administrateur</th>
								@if (session('user')->is_admin_deca == 1)
									<th>Admin Deca</th>
								@endif
							</tr>
						</thead>
						<tbody>
						@foreach( $users as $user )
							<tr>
								<td>
									{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('users.destroy', $user->email))) !!}
									{!! link_to_route('users.edit', 'Modifier', array($user->email), array('class' => 'btn btn-info')) !!}
									@if (session('user')->is_admin_deca == 1)
										{!! Form::submit('Supprimer', array('class' => 'btn btn-danger')) !!}
									@endif
									{!! Form::close() !!}
								</td>
								<td>{{ $user->firstname or ''}}</td>
								<td>{{ $user->lastname or '' }}</td>
								<td>{{ $user->email or '' }}</td>
								<td>{{ $user->is_admin or '' }}</td>
								@if (session('user')->is_admin_deca == 1)
									<td>{{ $user->is_admin_deca or '' }}</td>
								@endif
							</tr>
						@endforeach 
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
		$('#tableUser').dataTable( {
		language: {
			processing:     "Traitement en cours...",
			search:         "",
			lengthMenu:     "Afficher _MENU_",
			info:           "Utilisateurs _START_ &agrave; _END_ sur _TOTAL_",
			infoEmpty:      "",
			infoFiltered:   "(filtr&eacute; de _MAX_ utilisateurs au total)",
			infoPostFix:    "",
			loadingRecords: "Chargement en cours...",
			zeroRecords:    "Aucun utilisateur &agrave; afficher",
			emptyTable:     "Aucun utilisateur &agrave; afficher",
			paginate: {
				first:      "Premier",
				previous:   "Pr&eacute;c&eacute;dent",
				next:       "Suivant",
				last:       "Dernier"
			},
			aria: {
				sortAscending:  ": activer pour trier la colonne par ordre croissant",
				sortDescending: ": activer pour trier la colonne par ordre décroissant"
			}
		},
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
	} );
	$('#tableUser_filter input').attr("placeholder", "Rechercher");
	$('#tableUser_filter input').attr("class", "");
});
</script>

@endsection