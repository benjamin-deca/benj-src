@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="users">Utilisateurs</a></li>
			<li><a href="users/create">Nouvel utilisateur</a></li>
		</ol>
	</div>
</div>

{!! Form::model(new App\User, ['route' => ['users.store']]) !!}
	{!! csrf_field() !!}
    @include('users/partials/_form', ['submit_text' => 'Enregistrer'])
{!! Form::close() !!}

@endsection