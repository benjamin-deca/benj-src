@extends('layout')
@section('content')
<div id="dialogForm" class="modal fade" role="dialog"></div>

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="home">Accueil</a></li>
			<li><a href="users">Utilisateurs</a></li>
			<li><a href="users/{{$User->email or ''}}/edit">{{$User->firstname or ''}} {{$User->lastname or ''}}</a></li>
			
		</ol>
	</div>
</div>

{!! Form::model($User, ['method' => 'PATCH', 'route' => ['users.update', $User->email]]) !!}
	{!! csrf_field() !!}
	@include('users/partials/_form', ['submit_text' => 'Enregistrer'])
{!! Form::close() !!}

<script type="text/javascript">
$(document).ready(function() {
	$("#updatePwd").on("click", function() {
		$('#dialogForm').load('updatePwd #dialogPwd', function(responseTxt, statusTxt, xhr){
			$('#dialogForm').modal('show');
			$.getScript('js/userPwd.js');
		});
	});
});
</script>
@endsection