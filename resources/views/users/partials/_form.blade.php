<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Utilisateur</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">{{$User->title or ''}} {{$User->firstname or 'Nouvel utilisateur'}} {{$User->lastname or ''}}</h4>
				<!-- TODO Finaliser la gestion de l'avatar -->
				<div class="row form-group">
					@if (session('user')->is_admin_deca == 1)
					<label class="col-sm-3 control-label">Portail</label>
					<div class="col-sm-5">
						<select class="form-control" name="council_id" id="council_id">
							@foreach($councils as $council)
								<option value="{{ $council->id }}" {{ isset($User->council_id) ? (($User->council_id == $council->id) ? 'selected' : '') : '' }}>{{ $council->fullname }}</option>
							@endforeach
						</select>
					</div>
						@else
							<input type="hidden" name="council_id" id="council_id" value="{{ $User->council_id or '1' }}" />
						@endif
				</div>
					@if ($errors->has('title'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
						<label class="col-sm-3 control-label">Civilit&eacute; *</label>
						<div class="col-sm-3">
							<select class="form-control" name="title" value="{{$User->title or ''}}">
								<option value="">Non renseign&eacute;e</option>
								<option value="M" {{ isset($User->title) ? (($User->title == 'M') ? 'selected' : '') : '' }}>Monsieur</option>
								<option value="MME" {{ isset($User->title) ? (($User->title == 'MME') ? 'selected' : '') : '' }}>Madame</option>
								<option value="MLLE" {{ isset($User->title) ? (($User->title == 'MLLE') ? 'selected' : '') : '' }}>Mademoiselle</option>
							</select>
						</div>
						@if ($errors->has('title'))
							@foreach ($errors->get('title') as $error)
								<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					@if ($errors->has('lastname'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
						<label class="col-sm-3 control-label">Nom *</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="lastname" id="lastname" value="{{$User->lastname or ''}}" />
						</div>
						@if ($errors->has('lastname'))
							@foreach ($errors->get('lastname') as $error)
								<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					@if ($errors->has('firstname'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
						<label class="col-sm-3 control-label">Pr&eacute;nom *</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="firstname" id="firstname" value="{{$User->firstname or ''}}" />
						</div>
						@if ($errors->has('firstname'))
							@foreach ($errors->get('firstname') as $error)
								<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					@if ($errors->has('email'))
						<div class="row form-group has-error">
					@else
						<div class="row form-group">
					@endif
						<label class="col-sm-3 control-label">Email *</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="email" id="email" value="{{$User->email or ''}}" />
						</div>
						@if ($errors->has('email'))
							@foreach ($errors->get('email') as $error)
								<small class="help-block col-sm-5 col-sm-offset-3">{{ $error }}</small>
							@endforeach
						@endif
					</div>
					<div class="row form-group">
						@if (session('user')->is_admin_deca == 1)
							<label class="col-sm-3 control-label">Code externe CRM</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="externalcrm_id" id="externalcrm_id" value="{{$User->externalcrm_id or ''}}" />
							</div>
						@else
						<input type="hidden" name="externalcrm_id" id="externalcrm_id" value=" {{ $User->externalcrm_id or '' }}" />
						@endif
					</div>
					<div class="row form-group">
						@if ((session('user')->is_admin_deca == 1) || (session('user')->is_admin == 1))
						<label class="col-sm-3 control-label">Administrateur</label>
						<div class="col-sm-5">
							<div class="toggle-switch toggle-switch-success">
								<label>
									<input type="checkbox" name="is_admin" id="is_admin" {{ isset($User->is_admin) ? (($User->is_admin == 1) ? 'checked' : '') : '' }} />
									<div class="toggle-switch-inner"></div>
									<div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
								</label>
							</div>
						</div>
						@else
							<input type="hidden" name="is_admin" id="is_admin" value="{{ $User->is_admin or '0' }}" />
						@endif
					</div>
					<div class="row form-group">
						@if (session('user')->is_admin_deca == 1)
							<label class="col-sm-3 control-label">Administrateur Decacity</label>
							<div class="col-sm-5">
								<div class="toggle-switch toggle-switch-success">
									<label>
										<input type="checkbox" name="is_admin_deca" id="is_admin_deca" {{ isset($User->is_admin_deca) ? (($User->is_admin_deca == 1) ? 'checked' : '') : '' }} />
										<div class="toggle-switch-inner"></div>
										<div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
									</label>
								</div>
							</div>
						@else
							<input type="hidden" name="is_admin_deca" id="is_admin_deca" value="{{ $User->is_admin_deca or '0' }}" />
						@endif
					</div>
			</div>
		</div>
	</div>
	@if (session('portalConfig.MOD_AVATAR_ACTIVATED') == 1)
	<div class="col-xs-12 col-sm-12 col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-file-image-o"></i>
					<span>Avatar</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<img src='' />
				<br />
				<button type="button" id="updateAvatar" class="btn btn-primary">
					<span>
						<i class="fa fa-file-image-o"></i> 
						Modifier l'avatar
					</span>
				</button>
			</div>
		</div>
	</div>
	@endif
	<div class="row form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-2">
		{!! Form::button('<i class="fa fa-pencil-square-o"></i> Modifier mon mot de passe', ['id' => 'updatePwd', 'name' => 'updatePwd', 'class'=>'btn btn-primary']) !!}
		{!! Form::button('<i class="fa fa-floppy-o"></i> '. $submit_text, ['type' => 'submit', 'class'=>'btn btn-success']) !!}
	</div>
</div>