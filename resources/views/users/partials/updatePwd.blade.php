<!-- Boite de dialogue "Modifier votre mot de passe" -->
<div id="dialogPwd" class="modal-dialog">
	<div class="modal-content">     
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modifier votre mot de passe</h4>
		</div>
		<div class="modal-body">
			<form>
			{!! csrf_field() !!}
				@if (isset($_POST['password']))
				<div class="row form-group has-error">
					@foreach ($_POST['password'] as $error)
						<small class="help-block col-sm-10">{{ $error }}</small>
						<br>
					@endforeach
				</div>
				@endif
				<fieldset>
					@if (isset($_POST['password']))
					<div class="row form-group has-error">
					@else
					<div class="row form-group">
					@endif
						<label class="col-sm-4 control-label">Mot de passe *</label>
						<div class="col-sm-7">
							<input type="password" class="form-control" name="password" id="password_form" />
						</div>
					</div>
					@if (isset($_POST['password']))
					<div class="row form-group has-error">
					@else
					<div class="row form-group">
					@endif
						<label class="col-sm-4 control-label">Confirmation *</label>
						<div class="col-sm-7">
							<input type="password" class="form-control" name="password_confirmation" id="password_form_confirmation" />
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="modal-footer">
			<button id="submitPwd" type="button" class="btn btn-primary">Modifier votre mot de passe</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>

<!-- Boite de dialogue "Confirmation mot de passe modifié" -->
<div id="dialogPwdOk" class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Votre mot de passe</h4>
		</div>
		<div class="modal-body">
			<p>
				Votre mot de passe a bien &eacute;t&eacute; modifi&eacute;. Une notification a &eacute;t&eacute; envoy&eacute;e à votre adresse email.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
		</div>
	</div>
</div>