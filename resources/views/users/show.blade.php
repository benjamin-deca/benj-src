@extends('layout')

@section('content')

<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb">
			<li><a href="./home">Accueil</a></li>
			<li><a href="./users">Utilisateurs</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-home"></i>
					<span>Utilisateur</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="card">
					<h4 class="page-header">Utilisateur</h4>
					<p>
						<div class="form-group">
							<label class="col-sm-3 control-label">Civilit&eacute;</label>
							<div class="col-sm-5">
								{{$User->title or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Pr&eacute;nom</label>
							<div class="col-sm-5">
								{{$User->firstname or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Nom</label>
							<div class="col-sm-5">
								{{$User->lastname or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-5">
								{{$User->email or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Code externe CRM</label>
							<div class="col-sm-5">
								{{$User->externalcrm_id or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Administrateur</label>
							<div class="col-sm-5">
								{{$User->is_admin or ''}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Administrateur Deca</label>
							<div class="col-sm-5">
								{{$User->is_admin_deca or ''}}
							</div>
						</div>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@stop