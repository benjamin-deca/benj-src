<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class InteractsTest extends TestCase
{
    //clicks part
    public function testClickServices()
    {
        $this->visit('/')
         ->click('Accéder aux services')
         ->seePageIs('/auth/login');
            
    }
    public function testClickRegister(){
        $this->visit('/')
            ->click('Créer un compte')
            ->seePageIs('/auth/register');
    }

    public function testClickRegisterFromLogin(){
        $this->visit('/auth/login')
            ->click('Créer un compte ?')
            ->seePageIs('/auth/register');
    }
     public function testClickLoginFromRegister(){
        $this->visit('/auth/login')
            ->click("J'ai déjà un compte")
            ->seePageIs('/auth/login');
    }   


    //form part
    public function testFormAuthLoginFail(){
        $this->visit('/auth/login')
        ->type('EMAILNONREFERENCERENBDD@AAA.COM','email')
        ->type('123456','password')
        ->press('Se connecter')
        ->seePageIs('/auth/login');
    }
    public function testFormAuthLoginValid(){
        $user = factory(App\User::class, 'user_deca')->create();
        $this->actingAs($user)
         ->withSession(['foo' => 'bar'])
        ->visit('/auth/login')
        ->type('testing@decacity.com','email')
        ->type('27091987','password')
        ->press('Se connecter')
        ->seePageIs('/home');
    }

    public function testFormAuthRegister(){

    }


}
