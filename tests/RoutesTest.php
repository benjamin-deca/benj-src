<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class RoutesTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testGet()
    {
		//$this->assertTrue(true);
        $this->visit('/')
			->see('Decacity')
			->see('Portail de services citoyens')
            ->see('Acc&eacute;der aux services')
            ->see('Cr&eacute;er un compte')
            ->see('Accessibilit&eacute;')
            ->see('Mentions l&eacute;gales');
			
    }
    public function testGetAuthLogin(){
        $this->visit('/auth/login')
            ->see('Accueil')
            ->see('Cr&eacute;er un compte ?')
            ->see('Connexion')
            ->see("Nom d'utilisateur (Email)")
            ->see('Mot de passe')
            ->see('Mot de passe perdu ?')
            ->see('Se souvenir de moi ?')
            ->see('Se connecter');
    }
}
